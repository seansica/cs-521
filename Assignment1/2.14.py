#!/usr/bin/env python

"""
2.14.py: (Geometry: area of a triangle)

Write a program that prompts the user to enter the three points (x1, y1), (x2, y2), and (x3, y3) of a triangle and
displays its area. The formula for computing the area of a triangle is
s = (side1 + side2 + side3)/2
area = sqrt(s(s - side1)(s - side2)(s - side3))
"""

import sys
import math

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

REQUIRED_NUMBER_OF_POINTS = 6


def main():
    """
    Calculates the area of a triangle
    """
    # Collect user input and validate input is valid
    coordinates = validate_input(input("Enter three points for a triangle: "))

    # Set data points
    x1 = float(coordinates[0])
    y1 = float(coordinates[1])
    x2 = float(coordinates[2])
    y2 = float(coordinates[3])
    x3 = float(coordinates[4])
    y3 = float(coordinates[5])

    # Calculate length of each side using the Pythagorean theorem
    side_a = math.sqrt(((x2 - x3) ** 2) + ((y2 - y3) ** 2))
    side_b = math.sqrt(((x1 - x3) ** 2) + ((y1 - y3) ** 2))
    side_c = math.sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2))
    # print("side_a=", side_a)
    # print("side_b=", side_b)
    # print("side_c=", side_c)

    # Calculate semi-perimeter
    s = (side_a + side_b + side_c) / 2
    # print("s=", s)

    # Calculate area using Heron's formula
    area = math.sqrt(s * (s - side_a) * (s - side_b) * (s - side_c))

    # Print the result to console
    print("The area of the triangle is", round(area, 2))


def validate_input(points):
    """
    Validate that user input is a valid integer.
    Supports real and rational numbers.
    Rounds down to nearest whole number.
    """
    try:
        # Convert user input to list of values
        coordinates = str(points).split(",")

        # Ensure exactly six values entered
        if len(coordinates) > REQUIRED_NUMBER_OF_POINTS:
            raise ValueError("Invalid input. Too many points entered.")

        if len(coordinates) < REQUIRED_NUMBER_OF_POINTS:
            raise ValueError("Invalid input. Too few points entered.")

        # Ensure each point is a valid number; use float to account for rational numbers
        for pt in coordinates:
            if not isinstance(float(pt), float):
                raise ValueError("Invalid input. Please enter only numbers.")

        # Returns a list of string objects
        return coordinates

    except ValueError as error:
        print(error.args)
        sys.exit(0)


# Makes script executable as a python program
if __name__ == "__main__":
    main()
