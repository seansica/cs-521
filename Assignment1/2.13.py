#!/usr/bin/env python

"""
2.13.py: (Split digits)

Write a program that prompts the user to enter a four-digit integer and displays the number in reverse order.
"""

import sys
import math

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

REQUIRED_INT_LENGTH = 4


def main():
    """
    Prints each character in reverse from the user-defined integer separated by newline breaks
    """
    # Prompt the user to input a number
    user_defined_number = validate_integer(input("Enter a 4-digit number:"))

    # Iterate over each integer in the reversed user-defined number list and print to console
    for i in reversed(str(user_defined_number)):
        print(i)


def validate_integer(number):
    """
    Validate that user input is a valid integer.
    Supports real and rational numbers.
    Rounds down to nearest whole number.
    """
    try:
        value = int(number)

        # Fail if user entered integer shorter or longer than 4 digits
        if REQUIRED_INT_LENGTH > len(str(value)) or REQUIRED_INT_LENGTH < len(str(value)):
            print("Invalid input. Integer must be 4 digits long.")
            sys.exit(0)

        return math.floor(value)

    except ValueError:
        print("Invalid input. Please enter only numbers.")
        sys.exit(0)


# Makes script executable as a python program
if __name__ == "__main__":
    main()
