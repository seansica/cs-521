#!/usr/bin/env python

"""
2.7.py: (Find the number of years and days)

Write a program that prompts the user to enter the minutes (e.g., 1 billion),
and displays the number of years and days for the minutes. For simplicity, assume a year has 365 days.
"""

import sys
import math

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

MINUTES_PER_DAY = 1440     # Equals 60 * 24
MINUTES_PER_YEAR = 525600  # Equals 60 * 24 * 365


def main():
    """
    Displays the number of years and days for the user-defined minutes value.
    """
    raw_input = input("Enter number of minutes:")

    # Validate that user input is a valid integer.
    # Supports real and rational numbers.
    try:
        user_defined_minutes = int(raw_input)
    except ValueError:
        print("Invalid input. Please enter only whole numbers.")
        sys.exit(0)

    # Convert minutes value to years value
    number_of_years = int(user_defined_minutes) / MINUTES_PER_YEAR

    # Use modulo function to extract the number of remaining days from the years value
    # i.e. math.floor(1900.5 years) returns (0.5 * 365) or 182.5 days
    try:
        number_of_days = (number_of_years % math.floor(number_of_years)) * 365
    except ZeroDivisionError:
        # This exception is here to catch instances where math.floor(x) returns 0, meaning that the number of minutes
        # that the user entered was less than one year. This case would trigger a float modulo error because you cannot
        # divide by zero.
        number_of_days = math.floor(user_defined_minutes / MINUTES_PER_DAY)

    # Use math.floor to round year down
    # i.e. round(1905.5 years) returns 1905 years
    print(user_defined_minutes, "minutes is equal to approx.", math.floor(number_of_years), "years and",
          round(number_of_days), "days.")


# Makes script executable as a python program
if __name__ == "__main__":
    main()
