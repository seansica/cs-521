#!/usr/bin/env python

"""
2.22.py: (Population projection)

Rewrite Exercise 1.11 to prompt the user to enter the number of years and displays the population after that many years.

Exercise 1.11:
The US Census Bureau projects population based on the following assumptions:
- One birth every 7 seconds
- One death every 13 seconds
- One new immigrant every 45 seconds

Write a program to display the population for each of the next five years.

Assume the current population is 312032486 and one year has 365 days. Hint: in Python, you can use integer division
operator // to perform division. The result is an integer.

For example, (5 // 4) is 1 (not 1.25) and (10 // 4) is 2 (not 2.5).
"""

import sys

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

BIRTH_RATE_SECONDS = 7
DEATH_RATE_SECONDS = 13
IMMIGRANT_RATE_SECONDS = 45
SECONDS_PER_YEAR = 60 * 60 * 24 * 365
CURRENT_POPULATION = 312032486


def main():
    """
    Calculate population rates and print annual population delta shift n times, based on the number of years indicated
    by the user.
    """
    # Instantiate the total number of years that the population should be calculated for.
    max_number_of_years = validate_float(input("Enter number of years:"))

    # Set the starting population total. This number will change with each iteration of the year.
    current_population = CURRENT_POPULATION

    # Pre-calculate the total number of deaths that occur each year
    annual_death_rate = SECONDS_PER_YEAR // DEATH_RATE_SECONDS  #

    # Pre-calculate the total number of births that occur each year
    annual_birth_rate = SECONDS_PER_YEAR // BIRTH_RATE_SECONDS

    # Pre-calculate the total number of new citizens per year
    annual_immigration_rate = SECONDS_PER_YEAR // IMMIGRANT_RATE_SECONDS

    # Set the starting year to 1. This number will increment +1 per loop, and will stop after n years.
    current_year = 1

    # Print starting values
    print("Deaths:", annual_death_rate, "Births:", annual_birth_rate, "Immigrants:", annual_immigration_rate)
    print("Starting population:", current_population)

    # Start looping over each year
    while current_year <= max_number_of_years:

        # Population after one year equals the running total, minus total deaths, plus total births, plus new immigrants
        current_population = current_population - annual_death_rate + annual_birth_rate + annual_immigration_rate

        # The formatter, f{value','}, signals the use of a comma for a thousands separator to help readability.
        print("Year:", current_year, ":: Population:", f'{current_population:,}')

        # Increment year
        current_year += 1


def validate_float(number):
    """
    Validate that user input is a valid integer.
    Supports real and rational numbers.
    Rounds down to nearest whole number.
    """
    try:
        return float(number)
    except ValueError:
        print("Invalid input. Please enter only numbers.")
        sys.exit(0)


# Make script executable as a python program
if __name__ == "__main__":
    main()
