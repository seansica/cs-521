#!/usr/bin/env python

"""
2.21.py: (Financial application: compound value)

Suppose you save $100 each month into a savings account with an annual interest rate of 5%.
Therefore, the monthly interest rate is 0.05/12 = 0.00417.

After the first month, the value in the account becomes
100 * (1 + 0.00417) = 100.417

After the second month, the value in the account becomes
(100 + 100.417) * (1 + 0.00417) = 201.252

After the third month, the value in the account becomes
(100 + 201.252) * (1 + 0.00417) = 302.507

and so on.

Write a program that prompts the user to enter a monthly saving amount and displays the account value after
the sixth month.
"""

import sys

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

MONTHLY_INTEREST_RATE = 1 + 0.05/12
NUMBER_OF_MONTHS = 6


def main():
    """
    Calculate total savings based on a user-defined initial savings after six months of interest accumulation.
    FORMULA == (MONTHLY_SAVING_AMOUNT + CURRENT_SAVINGS_AMOUNT) * (MONTHLY_INTEREST_RATE)
    """

    # Input initial savings
    raw_input = input("Enter current savings:")

    # Validate user input to ensure only valid type entered
    monthly_savings = validate_float(raw_input)

    current_savings = 0 # represents the month-to-month running savings total

    i = 0   # month counter
    while i != NUMBER_OF_MONTHS:
        current_savings = (monthly_savings + current_savings) * MONTHLY_INTEREST_RATE
        i += 1  # increment next month

    # Print final results. Round current_savings to two decimal places.
    print("After month", NUMBER_OF_MONTHS, "the account value is", round(current_savings, 2))


def validate_float(number):
    """
    Validate that user input is a valid integer.
    Supports real and rational numbers.
    Rounds down to nearest whole number.
    """
    try:
        return float(number)
    except ValueError:
        print("Invalid input. Please enter only numbers.")
        sys.exit(0)


# Make script executable as a python program
if __name__ == "__main__":
    main()
