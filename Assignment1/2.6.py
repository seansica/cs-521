#!/usr/bin/env python

"""
2.6.py: Sums the digits in an integer

Write a program that reads an integer between 0 and 1000 and adds all the digits in the integer.
For example, if an integer is 932, the sum of all its digits is 14.
(Hint: Use the % operator to extract digits, and use the // operator to remove the extracted digit.
For instance, 932 % 10 = 2 and 932 // 10 = 93.)
"""

import sys
import math

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


MIN_NUMBER = 0
MAX_NUMBER = 1000


def main():

    user_defined_number = validate_integer(input("Enter number between 0 and 1000: "))

    # always start counter at zero, will use this to keep track of sum
    count = 0

    # iterate over each int(char) in the given integer and add to the counter
    for number in str(user_defined_number):
        count += int(number)

    # contains the final result
    print("SUM:", count)
    return count


def validate_integer(number):
    """
    Validate that user input is a valid integer.
    Supports real and rational numbers.
    Rounds down to nearest whole number.
    """
    try:
        value = int(number)

        # Fail if user entered integer shorter or longer than 4 digits
        if MIN_NUMBER > value or MAX_NUMBER < value:
            print("Invalid input. Integer must be between 0 and 1000.")
            sys.exit(0)

        return math.floor(value)

    except ValueError:
        print("Invalid input. Please enter only numbers.")
        sys.exit(0)


# Makes script executable as a python program
if __name__ == "__main__":
    main()
