#!/usr/bin/env python

"""
4.33.py (Decimal to hex)

Write a program that prompts the user to enter an integer between 0 and 15 and displays its corresponding hex number.
"""

import sys

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def is_hex_char(integer: int):
    """
    Ensures user-defined value is a single hexadecimal value from 0-15
    :param integer: User-defined parameter
    :return: Return the integer if the integer is a single hex value from 0-15, otherwise end program
    """
    if 0 > integer or integer > 15:
        print("Error! Integer must be between 0-15.")
        sys.exit(0)

    return integer


def main():
    """
    Reads in a decimal value from the console and attempts to convert it to a single hexadecimal value 0-15
    """
    try:
        print("The hex value is", format(is_hex_char(int(input("Enter a decimal value (0 to 15):"))), 'X'))

    except ValueError:
        print("Error! Please enter an integer.")
        sys.exit(0)


# Make script executable as a python program
if __name__ == "__main__":
    main()
