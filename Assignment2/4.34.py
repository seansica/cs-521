#!/usr/bin/env python

"""
4.34.py (Hex to decimal)

Write a program that prompts the user to enter a hex character and displays its corresponding decimal integer.
"""

import sys

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def decimal_value(value: str):
    """
    Ensures user-defined value is a single hexadecimal value from 0-15
    :param value: User-defined parameter
    :return: Return the integer if the integer is a single hex value from 0-15, otherwise end program
    """
    # Validate that hex value is between 0-15
    if 0 > int(value, 16) or int(value, 16) > 15:
        print("Error! Value must be between 0-15d.")
        sys.exit(0)

    # Return a base-10 formatted decimal value
    return format(int(value, 16), 'd')


def main():
    """
    Reads in a hex value from the console and attempts to convert it to a single decimal character (0-15,A-F)
    """
    try:
        print("The decimal value is", decimal_value(input("Enter a hex character:")))

    except ValueError:
        print("Error! Please enter an hexadecimal character (0-15,A-F).")
        sys.exit(0)


# Make script executable as a python program
if __name__ == "__main__":
    main()
