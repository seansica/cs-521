#!/usr/bin/env python

"""
4.21.py: (Science: day of the week) Zeller’s congruence is an algorithm developed by Christian Zeller to calculate the
day of the week. The formula is:

h = (q + (26(m+1)/10) + k + (k/4) + (j/4) + 5j) % 7

where:
- h is the day of the week (0: Saturday, 1: Sunday, 2: Monday, 3: Tuesday, 4: Wednesday, 5: Thursday, 6: Friday).
- q is the day of the month.
- m is the month (3:March, 4:April, ..., 12:December). January and February are counted as months 13 and 14 of the
  previous year.
- j is the century (i.e., year/100).
- k is the year of the century (i.e., year % 100).

Write a program that prompts the user to enter a year, month, and day of the month, and then it displays the name of
the day of the week.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

MAX_YEAR = 3000
MIN_YEAR = 0


def is_year(year: int):
    """
    Validates that the year is in the valid year range, 0-3000
    :param year: Year to be scrutinized
    :return: If True, return the year. If False, raise a ValueError
    """
    if year > MAX_YEAR:
        return ValueError("Year exceeds maximum year allowed (" + str(MAX_YEAR) + ")")
    if MIN_YEAR > year:
        return ValueError("Year does not meet minimum year threshold (" + str(MIN_YEAR) + ")")
    else:
        return year


def is_month(month: int):
    """
    Validates that the month is in the valid month range 1-12
    :param month: Month to be scrutinized
    :return: If True, return the month. If False, raise a ValueError
    """
    if 1 > month > 12:
        return ValueError("Month must between between 1 and 12.")
    return month


def is_day(day: int):
    """
    Validates that the date is in the valid date range 1-31
    :param day: Date to be scrutinized
    :return: If True, return the date. If False, raise a ValueError
    """
    if 1 > day > 31:
        return ValueError("Month must between between 1 and 31.")
    return day


def day_of_week(h):
    """
    Maps Zeller’s congruence results to days of the week
    :param h: Result of Zeller’s congruence
    :return: Day of the week
    """
    return {
        0: "Saturday",
        1: "Sunday",
        2: "Monday",
        3: "Tuesday",
        4: "Wednesday",
        5: "Thursday",
        6: "Friday",
    }[h]


def main():
    # Collect user input
    year = is_year(int(input("Enter year [0-2030]:")))
    month = is_month(int(input("Enter month [0-12]:")))  # m
    day_of_month = is_day(int(input("Enter the day of the month [1-31]:")))  # q

    # January and February are counted as months 13 and 14 of the previous year.
    if month == 1:
        month = 13
        year = year - 1
    elif month == 2:
        month = 14
        year = year - 1

    # Calculate required vars
    j = year // 100
    k = year % 100
    m = month
    q = day_of_month

    # Calculate Zeller’s congruence
    h = (q + 13 * (m + 1) // 5 + k + k // 4 + j // 4 + 5 * j) % 7

    print("Day of the week is", day_of_week(h))


# Make script executable as a python program
if __name__ == "__main__":
    main()
