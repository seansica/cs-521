#!/usr/bin/env python

"""
3.9.py: (Financial application: payroll)

Write a program that reads the following information and prints a payroll statement:
Employee’s name (e.g., Smith)
Number of hours worked in a week (e.g., 10) Hourly pay rate (e.g., 9.75)
Federal tax withholding rate (e.g., 20%) State tax withholding rate (e.g., 9%)
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def main():
    employee = add_payroll()
    get_payroll(employee)
    return


def is_rate(rate: float):
    """
    Validates that the tax withholding rate is represented as a fractional percentage.
    :param rate: The employee's tax withholding rate
    :return: If True, return the employee's tax withholding rate. If False, raise a ValueError.
    """
    if 0 < rate < 1:
        return rate
    else:
        raise ValueError("Error! Rate must be a fraction between 0 and 1")


def add_payroll():
    """
    Records payroll attributes from console input and stores in a dict
    :return: dict containing the specified parameters of an employees payroll attributes
    """
    # Declare global vars
    hourly_pay_rate, hours_worked_per_week, federal_tax_withholding_rate, state_tax_withholding_rate, total_deductions = 0, 0, 0, 0, 0

    # Collect user input to define employee name.
    name = input("Enter employee's name:")

    # Collect user input to define employee attributes. Raise ValueError if user-defined values violate required
    # data types.
    try:
        hours_worked_per_week = float(input("Enter number of hours worked in a week:"))
        hourly_pay_rate = float(input("Enter hourly pay rate:"))
        federal_tax_withholding_rate = is_rate(float(input("Enter federal tax withholding rate:")))
        state_tax_withholding_rate = is_rate(float(input("Enter state tax withholding rate:")))

    except ValueError:
        print("Error! Enter numerical digits only.")

    # Calculate gross pay as the product of the employees hourly rate and the total number of hours worked
    gross_pay = hourly_pay_rate * hours_worked_per_week

    # Calculate federal tax withholding
    total_federal_withheld = calculate_withholding(gross_pay, federal_tax_withholding_rate)

    # Calculate state tax withholding
    total_state_withheld = calculate_withholding(gross_pay, state_tax_withholding_rate)

    # Calculate total deductions
    total_deductions = total_federal_withheld + total_state_withheld

    # Calculate net pay
    net_pay = calculate_net(gross_pay, total_federal_withheld, total_state_withheld)

    # Build and return dictionary of employee attributes
    employee = {"name": name, "hours": hours_worked_per_week, "pay_rate": hourly_pay_rate,
                "federal_tax_rate": federal_tax_withholding_rate, "state_tax_rate": state_tax_withholding_rate,
                "fed_withheld": total_federal_withheld, "state_withheld": total_state_withheld,
                "total_deductions": total_deductions, "gross": gross_pay, "net": net_pay}

    return employee


def calculate_net(gross: int, fed_withholding: int, state_withholding: int):
    """
    Calculates the net pay.
    :param gross: The employee's gross pay
    :param fed_withholding: The employee's federal tax withholding rate
    :param state_withholding: The employee's state tax withholding rate
    :return: The employee's net pay
    """
    return gross - fed_withholding - state_withholding


def calculate_withholding(gross: int, rate: int):
    """
    Calculate the tax withholding rate.
    :param gross: The employee's gross pay
    :param rate: The employee's federal or state tax withholding rate
    :return: The employee's total taxes withheld
    """
    return gross * rate


def get_payroll(employee: dict):
    """
    Prints the employees attributes to the console.
    :param employee: Specifies the employee that should be scrutinized.
    :return: None
    """
    print("Employee Name:", employee.get("name"))
    print("Hours Worked:", employee.get("hours"))
    print("Pay Rate:", "$" + str(round(employee.get("pay_rate"), 2)))
    print("Gross Pay:", "$" + str(round(employee.get("gross"), 2)))
    print("Deductions:")
    print("  Federal Withholding:", "$" + str(round(employee.get("fed_withheld"), 2)))
    print("  State Withholding:", "$" + str(round(employee.get("state_withheld"), 2)))
    print("  Total Deductions:", "$" + str(round(employee.get("total_deductions"), 2)))
    print("Net Pay:", "$" + str(round(employee.get("net"), 2)))


# Make script executable as a python program
if __name__ == "__main__":
    main()
