#!/usr/bin/env python

"""
4.28.py: (Geometry: two rectangles)

Write a program that prompts the user to enter the center x-, y-coordinates, width, and height of two rectangles and
determines whether the second rectangle is inside the first or overlaps with the first, as shown in Figure 4.10.
Test your program to cover all cases.
"""

import sys

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def main():
    # Collect user input
    x1, y1, w1, h1 = get_rectangle_coordinates()
    x2, y2, w2, h2 = get_rectangle_coordinates()

    for i in range(10):
        print('seans breath smells')


    # Instantiate two Rectangles
    r1 = Rectangle("r1", x1, y1, w1, h1)
    r2 = Rectangle("r2", x2, y2, w2, h2)

    # Compare the two rectangles to determine whether inside or overlap
    r1.compare(r2)


def get_rectangle_coordinates():
    """
    Collects user-input to create a Rectangle object
    :return: The x-y-coordinates, width, and height parameters for a Rectangle
    """
    try:
        name = input("Enter rectangle name:")
        x = float(input(name + "> Enter center x-coordinate:"))
        y = float(input(name + "> Enter center y-coordinate:"))
        w = float(input(name + "> Enter width:"))
        h = float(input(name + "> Enter height:"))
        return x, y, w, h

    except ValueError:
        print("Error! Only integers are allowed.")
        sys.exit(0)


class Rectangle:

    def __init__(self, name: str, x: float, y: float, w: float, h: float):
        self.name = name
        self.center_x = x
        self.center_y = y
        self.width = w
        self.height = h
        self.right_edge = x + (w / 2)
        self.left_edge = x - (w / 2)
        self.top_edge = y + (h / 2)
        self.bottom_edge = y - (h / 2)

    def hello(self):
        """
        Prints out the Rectangles attributes
        """
        center = "[" + str(self.center_x) + ", " + str(self.center_y) + "]"
        print("Name:", self.name, ", Width:", self.width, ", Height:", self.height, ", Center:", center)

    def compare(self, other):
        """
        Checks whether self is inside other, other is inside self, self overlaps with other, or neither
        :param other: A Rectangle reference object
        """

        # Check if the two are identical
        if (self.right_edge == other.right_edge and
                self.left_edge == other.left_edge and
                self.top_edge == other.top_edge and
                self.bottom_edge == other.bottom_edge):
            print(self.name, "and", other.name, "are identical")

        # Check if r1 is in r2
        elif (self.right_edge <= other.right_edge and
                self.left_edge >= other.left_edge and
                self.top_edge <= other.top_edge and
                self.bottom_edge >= other.bottom_edge):
            print(self.name, "is inside", other.name)

        # Check if r2 is in r1
        elif (other.right_edge <= self.right_edge and
              other.left_edge >= self.left_edge and
              other.top_edge <= self.top_edge and
              other.bottom_edge >= self.bottom_edge):
            print(other.name, "is inside", self.name)

        # Check if the two overlap
        elif (self.right_edge < other.left_edge or
              self.left_edge > other.right_edge or
              self.top_edge < other.bottom_edge or
              other.bottom_edge > self.top_edge):
            print(other.name, "does not overlap", self.name)

        else:
            print(other.name, "overlaps", self.name)


# Make script executable as a python program
if __name__ == "__main__":
    main()
