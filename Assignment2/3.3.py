#!/usr/bin/env python

"""
3.3.py: (Geography: estimate areas)

Find the GPS locations for Atlanta, Georgia; Orlando, Florida; Savannah, Georgia; and Charlotte, North Carolina
from www.gps-data-team.com/map/ and compute the estimated area enclosed by these four cities. (Hint: Use the formula
in Programming Exercise 3.2 to compute the distance between two cities. Divide the polygon into two triangles and use
the formula in Programming Exercise 2.14 to compute the area of a triangle.)
"""

import math


__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

EARTH_RADIUS = 6371.01
ATLANTA = {"x": -84.5545400, "y": 33.7405800}  # ZIP 30336
ORLANDO = {"x": -81.5250400, "y": 28.4115300}  # ZIP 32836
SAVANNAH = {"x": -81.1998900, "y": 32.1672300}  # ZIP 31407
CHARLOTTE = {"x": -80.9567600, "y": 35.2072400}  # ZIP 28278


def main():
    d1 = get_distance(ATLANTA, ORLANDO)
    d2 = get_distance(ORLANDO, SAVANNAH)
    d3 = get_distance(SAVANNAH, CHARLOTTE)
    d4 = get_distance(CHARLOTTE, ATLANTA)
    d5 = get_distance(ATLANTA, SAVANNAH)

    area1 = get_area(d1, d2, d5)
    area2 = get_area(d3, d4, d5)

    area = area1 + area2
    print("AREA:", round(area, 2))
    return


def get_distance(src: dict, dst: dict):
    """
    The great circle distance is the distance between two points on the surface of a sphere. Let (x1, y1) and (x2, y2)
    be the geographical latitude and longitude of two points.The great circle distance between the two points can be
    computed using the following formula:

    d = radius * arccos(sin(x1) * sin(x2) + cos(x1) * cos(x2) * cos(y1 - y2))

    The average earth radius is 6,371.01 km. Note that you need to convert the degrees into radians using the
    math.radians function since the Python trigonometric functions use radians. The latitude and longitude degrees
    in the formula are for north and west. Use negative to indicate south and east degrees

    :param src: x,y coordinates of the starting city
    :param dst: x,y coordinates of the reference city
    :return: distance from starting/src city to reference/dst city
    """
    x1 = math.radians(src.get("x"))
    x2 = math.radians(dst.get("x"))
    y1 = math.radians(src.get("y"))
    y2 = math.radians(dst.get("y"))
    return EARTH_RADIUS * math.acos(math.sin(x1) * math.sin(x2)
                                    + math.cos(x1) * math.cos(x2)
                                    * math.cos(y1 - y2))


def get_area(side1: float, side2: float, side3: float):
    """
    The formula for computing the area of a triangle is

    s = (side1 + side2 + side3)/2

    area = sqrt(s(s - side1)(s - side2)(s - side3))

    :param side1: length of triangle side1
    :param side2: length of triangle side2
    :param side3: length of triangle side3
    :return: area of the triangle
    """
    s = (side1 + side2 + side3)/2
    area = math.sqrt(s * (s-side1) * (s - side2) * (s - side3))
    return area


# Make script executable as a python program
if __name__ == "__main__":
    main()
