#!/usr/bin/env python

"""
server.py: Server script to facilitate the exchange of messages between clients

The server script is a multi-threaded program which handles multiple, simultaneous socket connections to client sockets.
It echoes client messages to each other, effectively acting as a bridge between them, thereby alleviating clients from
having to maintain 1-to-many socket connections. Instead, all socket connections are centralized on the server.

In addition, the server offers several server-side features such as logging as client messages to a log file, monitoring
all connected client socket information, and gracefully shutting down socket connections to clients using a
client/server handshake exchange process wherein they pass & acknowledge a special KILL_SWITCH string message indicating
that the socket should be closed.
"""

import socket     # used to communicate between server and client sockets
import threading  # used for handling multiple tasks at once. (e.g. multiple client sockets, commands, etc.)
import os         # used to clear artifacts/text from screen
import sys        # used to end server program

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

DEFAULT_HOST = ''  # refers to the listening address
DEFAULT_PORT = 5501  # refers to the listening port
DEFAULT_BUFFER_SIZE = 4096  # refers to maximum number of characters supported in a single message
DEFAULT_MAX_QUEUE = 10  # refers to maximum number of concurrent waiting client sockets
DEFAULT_LOG_PATH = "./log.txt"  # refers to current directory, i.e., directory in which server script is running
KILL_SWITCH = "!quit"  # refers to the trigger message that will close the server socket and all client sockets
SERVER_BANNER = '''
#################################################
##                                             ##
##         CS-521 Socket Chat Server           ##
##                                             ##
#################################################\n
>> Enter 'help' to show list of supported server commands.'''


class Server:

    def __init__(self, host: str = DEFAULT_HOST, port: int = DEFAULT_PORT, bufSize: int = DEFAULT_BUFFER_SIZE,
                 logPath: str = DEFAULT_LOG_PATH):
        self.__host = host  # refers to the listening address, e.g. localhost, 127.0.0.1
        self.__port = port  # refers to the listening port, e.g. 1–65535
        self.__bufSize = bufSize  # refers to the maximum num of characters supported in a single message
        self.address = (self.__host, self.__port)  # instantiates a server socket tuple
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # instantiates a server socket
        self.connectedClientSockets = {}  # holds list of connected sockets, each in format: {socket: client_name}
        self.connectedClientAddresses = {}  # holds list of connected client addresses in format: {socket: (IP, PORT)}

        self.run = False  # used to control server dashboard loop. Ends program when false.

        self.__logPath = logPath  # refers to system dir path where logs should be written
        self.__loggingEnabled = False  # default state of logging messages to file is Disabled

        # Creates a thread to check whether new connections have been established by client sockets.
        self.ACCEPT_THREAD = threading.Thread(target=self.newConnectionListener)

        # Creates a thread to handle server menu commands
        self.MENU_THREAD = threading.Thread(target=self.serverDashboard)

    def __repr__(self):
        """
        Returns a printable representation of the server object
        :return: a printable representation of the server object
        """
        return repr("Server hosting address:    {}:{}\n"
                    "Connected clients:         {}:\n"
                    "Server connection status:  {}\n"
                    "Client max message buffer: {}\n".format(self.__host, self.__port,
                                                             self.connectedClientAddresses,
                                                             self.run,
                                                             self.__bufSize))

    def init(self):
        """
        Binds the server socket object to the predefined server addres and port, thereby officially initializing
        the server
        :return: None
        """
        self.run = True
        self.socket.bind(self.address)

        # Calling listen() makes the server socket ready to accept connections. (i.e., Calling listen() puts the
        # socket into server mode, whereas accept() waits for an incoming connection.)

        # DEFAULT_MAX_QUEUE refers to a maximum of N connections that the server will allow to queue up waiting for
        # socket connectivity. Any connection attempts subsequent to N (i.e., N+1 inclusive) will be refused.
        self.socket.listen(DEFAULT_MAX_QUEUE)

        # This thread is always running to check whether new connections have been established by client sockets.
        self.ACCEPT_THREAD.start()
        self.ACCEPT_THREAD.join()

    def newConnectionListener(self):
        """
        TODO
        This thread is always running to check whether new connections have been established by the client applications.
        :return:
        """
        # Starts the server menu handler thread.
        self.MENU_THREAD.start()

        # Always waiting to accept new connections.
        while True:
            # Accepting a client socket request returns a tuple in which the client IP and port resides
            # Accept incoming client requests and notify other users.

            client_socket, client_address = self.socket.accept()
            print(">> {}:{} is connected!".format(client_address[0], client_address[1]))

            # Server prompts the new client to enter a friendly name for themselves. The server will record and track
            # this name in the corresponding socket tuple.

            client_socket.send(">> Greetings! Please enter your name to continue:".encode())
            self.connectedClientAddresses[client_socket] = client_address

            # Instantiate a dedicated thread to handle the new client connection. The server maintains a unique client
            # thread for each connected client.

            CLIENT_THREAD = threading.Thread(target=self.clientListener, args=(client_socket,))
            CLIENT_THREAD.start()

    def serverDashboard(self):
        """
        This function is a thread which handles server menu operations. (Client sockets are handled by a different
        thread).
        :return:
        """

        def menuSelector(menuInput: str):
            """
            Facilitates selecting server operator menu options using switch-like logic statements
            :param menuInput: refers to the server-operator-defined menu option that will be processed
            :return: None
            """
            if menuInput == 'help':
                """Display list of supported server operator commands"""

                print('>> SEND  - Broadcast a message to the chat room.')
                print('>> LOG   - Enable client message logging to file.')
                print('>> CLEAR - Clear the screen.')
                print('>> HELP  - Displays list of supported server commands.')
                print('>> LIST  - Lists the current connections.')
                print('>> QUIT  - Shuts down the server.')

            elif menuInput == 'send':
                """Allows server operator to send message to all clients. Clients will see: SERVER: <message>"""

                message = input('What would you like to broadcast?\n>> ')
                self.broadcast("\nSERVER: ", message)

            elif menuInput == 'clear':
                """Clears the server console for readability"""
                clearScreen()

            elif menuInput == 'list':
                """Displays list of actively connected clients"""

                for client in self.connectedClientAddresses:

                    clientAddress = self.connectedClientAddresses[client][0]
                    clientPort = self.connectedClientAddresses[client][1]

                    # clientName may not set when user has entered room but has not entered a name yet. This
                    # handler will fail back to printing just their IP:port in such a case.
                    try:
                        clientName = self.connectedClientSockets[client]
                        print(">> Client: '{}' : {} : {}".format(clientName, clientAddress, clientPort))

                    except:
                        print('>> Connection: {} : {}'.format(clientAddress, clientPort))

            elif menuInput == 'log':
                """Facilitates toggling the logging functionality ON to allow client receiver threads to 
                spawn thread logs. This will ensure that all subsequent messages are written to log file."""

                self.__loggingEnabled = True
                print("Logging is now enabled!")
                print("Logs will be written to", self.__logPath)

            elif menuInput == 'quit':
                """Closes all client connections and quits the server"""

                for client in self.connectedClientAddresses:
                    clientAddress = self.connectedClientAddresses[client][0]
                    clientPort = self.connectedClientAddresses[client][1]
                    try:
                        clientName = self.connectedClientSockets[client]
                        print(">> Closing Client: '{}' : {} : {}".format(clientName, clientAddress, clientPort))
                    except:
                        print('>> Closing Client: {} : {}'.format(clientAddress, clientPort))

                    client.close()  # Close each client socket

                self.run = False  # End the program

            elif menuInput != '':
                """Exception handler for invalid/unknown commands"""
                print('>> Not a valid server command')

        # Infinitely loop until server process is killed. This is where the server operator lives.
        while self.run:
            cmd = input('>> ')
            menuSelector(cmd.lower())

        # Called when self.run becomes False, which can only occur when server operator enters 'quit'
        sys.exit(0)

    def clientListener(self, client):
        """
        This is a threaded function that listens for incoming data received by a client socket. There is one instance of
        this thread per connected client socket.
        :param client: refers to the client socket to which this thread belongs
        :return:
        """
        # Initially gets the name the client sends after loading the application and connecting
        # To the server and welcomes them to the chat room.
        client_name = client.recv(self.__bufSize).decode()
        client.send(
            "\n>> Welcome {}!\n>> To exit the chat room: enter '{}' in the chat.".format(client_name,
                                                                                         KILL_SWITCH).encode())

        # This calls the broadcast function to notify Who has joined the chat room.
        message = "\n>> {} Has joined the chat room!".format(client_name)
        self.broadcast(message)

        # This adds the clients name to the server's list of clients dictionary
        self.connectedClientSockets[client] = client_name

        # This loop is recur indefinitely until the user sends a request to quit.
        while True:
            try:
                # Attempt to receive data from the client socket, if any is queued up.
                message = client.recv(self.__bufSize).decode()

                # Close the client connection and kill the corresponding thread if the server receives the KILL_SWITCH
                # str from the client. Otherwise just broadcast the client message out to the other clients.
                if message == KILL_SWITCH:
                    self.closeClientConnection(client)
                    break

                if self.__loggingEnabled:
                    # TODO write the log thread function
                    # Create a thread to handle logging received messages to file
                    LOG_THREAD = threading.Thread(target=self.logListener(client_name, message))

                    # Indicates that the calling thread must wait for LOG_THREAD to finish until it finishes.
                    LOG_THREAD.join()
                    LOG_THREAD.start()

                sender = "\n" + client_name + ": "
                self.broadcast(sender, message)

            except:
                # If client has not sent data to its server thread, the recv() will fail, and the server will continue
                # polling for client data indefinitely. When the user sends data to the server socket, the recv() call
                # will succeed and the try block will execute.
                continue

    def logListener(self, sender: str = "", message: str = ""):
        """
        TODO
        :param sender:
        :param message:
        :return:
        """
        if self.__loggingEnabled:
            with open(self.__logPath, "a") as logFile:
                sender = "\n" + sender + ": "
                logFile.write(sender + message + "\n")
                logFile.close()

    def broadcast(self, sender: str, message: str = ''):
        """
        Facilitates broadcasting messages to all connected clients.
        :param message: refers to the chat message that the server will send to all connected clients
        :param sender: refers to the name of the client from which the message originated. It will be prefixed to the
                       message so the receiving clients know who it came from.
        :return: None
        """
        try:
            for user in self.connectedClientSockets:
                user.send("{}{}".format(sender, message).encode())
        except:
            pass

    def closeClientConnection(self, client):
        """
        Facilitates a graceful socket disconnect of a client and informs the remaining connected clients of the event
        :param client: refers to the client that is disconnecting from the server
        :return:
        """
        # Send the KILL_SWITCH str to client acknowledging that they are quitting
        client.send(KILL_SWITCH.encode())

        # Set reference variables to the client that is disconnecting for readability
        clientName = self.connectedClientSockets[client]
        clientAddr = self.connectedClientAddresses[client][0]
        clientPort = self.connectedClientAddresses[client][1]

        # Notify the remaining clients who are connected about the disconnected client
        self.broadcast("\n>> {} ({}:{}) has left the chat room.".format(clientName, clientAddr, clientPort))

        # Log the client disconnection to the server console
        print(">> Client: '{}' : {} : {} Has disconnected.".format(clientName, clientAddr, clientPort))

        # This closes the requested client connection.
        client.close()

        # Removes client from list of current connections
        del self.connectedClientAddresses[client]
        del self.connectedClientSockets[client]


def clearScreen():
    """
    Facilitates clearing all alphanumeric characters from the screen for readability. It accomplishes this by executing
    a primitive system call/command. The function attempts to be OS-agnostic by detecting the operating system.
    :return: None
    """
    if os.name == 'nt':
        # If operating system is Windows, it will send the 'cls' command to the command prompt.
        os.system('cls')
    else:
        # Otherwise, operating system is assumed to be a UNIX variant (macOS, Linux, etc.) and therefore invokes the
        # 'clear' command in the terminal.
        os.system('clear')

    # Always prints the default server banner after clearing the screen.
    print(SERVER_BANNER)


def main():
    clearScreen()  # Clear any preceding terminal artifacts before we start
    s = Server()  # Instantiate the server
    s.init()  # Initialize the server


# Make script executable as a python program
if __name__ == '__main__':
    main()
