#!/usr/bin/env python

"""
client.py: Client script to exchange messages with Server script

The Client script is a multi-threaded program which handles sending and receiving messages to a centralized
socket server.
"""

import socket  # used to establish socket connection to the server
import threading  # used to handle multiple tasks at once
import os  # used to clear artifacts/text from screen
import sys  # used to exit program

DEFAULT_HOST = 'localhost'
DEFAULT_PORT = 5501
DEFAULT_BUFFER_SIZE = 4096
DEFAULT_CONNECTED = False
KILL_SWITCH = "!quit"


class Client:

    def __init__(self, host: str = DEFAULT_HOST, port: int = DEFAULT_PORT, bufSize: int = DEFAULT_BUFFER_SIZE,
                 connectionStatus: bool = DEFAULT_CONNECTED):
        """
        Client constructor
        :param host: refers to a string containing either the IP address or resolvable DNS name of server
        :param port: refers to an integer containing the target socket port number of server
        :param bufSize: refers to the maximum number of characters the server will accept in each client message
        :param connectionStatus: refers to whether the client socket is actively connected to server socket
        """
        self.__host = host  # refers to the target server address; IP or FQDN supported
        self.__port = port  # refers to the target server port, e.g., 1–65535
        self.__bufSize = bufSize  # refers to the maximum number of characters supported in a single message
        self.__connected = connectionStatus  # tracks whether the client is actively connected to the server socket
        self.address = (self.__host, self.__port)  # client tuple used by server socket binding
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # client socket facilitates all communication

        # Create a thread to handle receiving messages
        self.RECEIVE_THREAD = threading.Thread(target=self.receiveListener)

        # Create a thread to handle sending messages
        self.SEND_THREAD = threading.Thread(target=self.sendListener)

    def __repr__(self):
        """
        Returns a printable representation of the client object
        :return: a printable representation of the client object
        """
        return repr("Client socket points to server: {}:{}\n"
                    "Client socket using address:  {}:{}\n"
                    "Server connection status:     {}\n"
                    "Client max message buffer:    {}\n".format(self.__host, self.__port,
                                                                self.socket.getsockname()[0],
                                                                self.socket.getsockname()[1],
                                                                self.__connected,
                                                                self.__bufSize))

    def connectToServer(self):
        """
        Facilitates initializing connection to server socket
        :return:
        """
        # Attempt to connect to the server
        try:
            self.socket.connect(self.address)
            self.__connected = True

        except OSError as err:
            print(repr(err))
            sys.exit()

        # Connection to server succeeded!

        print(">> Connected to Server.")
        input('>> Press enter to start...')
        assert self.__connected, "Cannot start receiver thread until client connects to server!"
        self.RECEIVE_THREAD.start()

    def receiveListener(self):
        """
        This is a threaded function which handles the task of receiving messages from the server.
        :return: None
        """
        if self.__connected:

            # Create a buffer for all messages received by the server.
            message_buffer = ''

            assert self.__connected, "Cannot start receiver thread until client connects to server!"
            self.SEND_THREAD.start()

            # Indefinitely loop until KILL_SWITCH str acknowledgement received from server
            while True:

                # Attempt to receive message from server socket
                try:
                    # Fetch the message and append to the message buffer.
                    message = self.socket.recv(self.__bufSize).decode()
                    message_buffer += message

                    # If client previously sent KILL_SWITCH command to server, it expects the server to echo it back
                    # to the client before the client closes its socket connection. This statement will trigger when
                    # server echoes back.

                    if message == KILL_SWITCH:
                        self.socket.close()
                        break

                    else:
                        clearScreen()
                        print(message_buffer)

                # Handle exception thrown when client ungracefully/preemptively closes socket connection
                except OSError as err:
                    print(repr(err))
                    break

    def sendListener(self):
        """
        Facilitates sending messages to other clients
        :return:
        """
        assert self.__connected, "Cannot start sender thread until client connects to server!"
        while True:

            # Get message input from user
            message = input(">> ")

            try:
                # If message matches the KILL_SWITCH string, then string is sent, and client send loop is broken.
                # Otherwise, loop is not broken (i.e., user chat continues).
                if message == KILL_SWITCH:
                    self.socket.send(message.encode())
                    break
                else:
                    self.socket.send(message.encode())

            # Exception will throw if server receives payload containing the KILL_SWITCH string, or if server shutdown
            # while client still connected.
            except:
                print("Connection to server lost")
                break


def clearScreen():
    """
    Facilitates clearing all alphanumeric characters from the screen for readability. It accomplishes this by executing
    a primitive system call/command. The function attempts to be OS-agnostic by detecting the operating system.
    :return: None
    """
    if os.name == 'nt':
        # If operating system is Windows, it will send the 'cls' command to the command prompt.
        os.system('cls')
    else:
        # Otherwise, operating system is assumed to be a UNIX variant (macOS, Linux, etc.) and therefore invokes the
        # 'clear' command in the terminal.
        os.system('clear')


def main():
    clearScreen()  # Clear any preceding terminal artifacts before we start
    client = Client()  # Instantiate the client
    client.connectToServer()  # Connect the client to the server


if __name__ == '__main__':
    main()
