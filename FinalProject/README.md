# CS-521 - Final Project
##### Topic: Multi-Threaded Client Server Socket Application
##### Author: Sean Sica
##### Due Date: Dec 7

### Summary
The goal or expected outcome of this project was to implement a networked chat application in which at least two clients could communicate with each other in a client-server model. Each client runs a client script on their system. Another system runs a server script. Each client system instantiates its own socket and connects itself to a corresponding socket on the server system. The server facilitates client-to-client socket communication.

### Threading
A fundamental challenge to completing this goal was implementing a server application that is capable of processing multiple clients at once. The server is required to simultaneously handle incoming client requests (e.g., new socket requests and new message processing requests). It must also be able to simultaneously handle server side operational functions such as echoing unidirectional messages from itself (the server) to all connected clients, tracking and monitoring all connected clients, and gracefully shutting down client connections.

The solution to implementing these parallel tasks was threading. The server object maintains dedicated threads for the following:
- A single thread to handle incoming connection requests from new clients
- A single static thread to handle all internal server operations (i.e., run the server operations dashboard)
- A dynamic child thread that handles all bidirectional socket communication between each client and the server. There exists one of these threads per client.
- A dynamic child thread to handles all log requests. There exists one of these threads per client-invoked message, only when the server has been explicitly instructed to log client messages.
![Threading Model](diagrams/thread_model.png)

### Libraries
The application was implemented using only built-in Python3 libraries. They are as follows:
- **socket** : Used to communicate between server and client sockets
- **threading** : Used for handling multiple tasks at once. (e.g. multiple client sockets, commands, etc.)
- **os** : Used to clear artifacts/text from screen
- **sys** : Used to end server program

### UML
![](diagrams/client_server_uml.png)
