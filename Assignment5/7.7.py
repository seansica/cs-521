#!/usr/bin/env python

"""
7.7.py: (Algebra: 2 * 2 linear equations)

Design a class named LinearEquation for a 2 * 2 system of linear equations:
ax+by=e, x=(ed-bf)/(ad-bc)
cx+dy=f, y=(af-ec)/(ad-bc)

The class contains:
■ The private data fields a, b, c, d, e, and f with get methods.
■ A constructor with the arguments for a, b, c, d, e, and f.
■ Six get methods for a, b, c, d, e, and f.
■ A method named isSolvable() that returns true if ad - bc is not 0.
■ The methods getX() and getY() that return the solution for the equation.

Draw the UML diagram for the class, and then implement the class. Write a test program that prompts the user to enter
a, b, c, d, e, and f and displays the result. If ad - bc is 0, report that “The equation has no solution.” See Exercise
4.3 for sample runs.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class LinearEquation:

    def __init__(self, a=0.0, b=0.0, c=0.0, d=0.0, e=0.0, f=0.0):
        self.__a = a
        self.__b = b
        self.__c = c
        self.__d = d
        self.__e = e
        self.__f = f

    def getA(self):
        """
        Accessor function for A value
        :return: A value
        """
        return self.__a

    def getB(self):
        """
        Accessor function for C value
        :return: B value
        """
        return self.__b

    def getC(self):
        """
        Accessor function for C value
        :return: C value
        """
        return self.__c

    def getD(self):
        """
        Accessor function for D value
        :return: D value
        """
        return self.__d

    def getE(self):
        """
        Accessor function for E value
        :return: E value
        """
        return self.__e

    def getF(self):
        """
        Accessor function for F value
        :return: F value
        """
        return self.__f

    def setA(self, a):
        """
        Mutator function for A value
        :param a: the new value for A
        :return: None
        """
        self.__a = a

    def setB(self, b):
        """
        Mutator function for B value
        :param b: the new value for B
        :return: None
        """
        self.__b = b

    def setC(self, c):
        """
        Mutator function for C value
        :param c: the new value for C
        :return: None
        """
        self.__c = c

    def setD(self, d):
        """
        Mutator function for D value
        :param d: the new value for D
        :return: None
        """
        self.__d = d

    def setE(self, e):
        """
        Mutator function for E value
        :param e: the new value for E
        :return: None
        """
        self.__e = e

    def setF(self, f):
        """
        Mutator function for f value
        :param f: the new value for F
        :return: None
        """
        self.__f = f

    def isSolvable(self):
        """
        Determines whether (ad-bc) is equal to zero
        :return: True if (ad-bc) is not equal to zero, otherwise False
        """
        return True if self.__a * self.__d - self.__b * self.__c != 0 else False

    def getX(self):
        """
        Computes X as the result of (ed-bf)/(ad-bc)
        :return: the computed X value
        """
        return (self.__e * self.__d - self.__b * self.__f)/(self.__a * self.__d - self.__b * self.__c)

    def getY(self):
        """
        Computes Y as the result of (af-ec)/(ad-bc)
        :return: the computed Y value
        """
        return (self.__a * self.__f - self.__e * self.__c)/(self.__a * self.__d - self.__b * self.__c)


def main():
    # Instantiate a linear equation object
    linear_equation = LinearEquation()

    # Set the linear equation variables from user input
    try:
        linear_equation.setA(float(input("Enter A:")))
        linear_equation.setB(float(input("Enter B:")))
        linear_equation.setC(float(input("Enter C:")))
        linear_equation.setD(float(input("Enter D:")))
        linear_equation.setE(float(input("Enter E:")))
        linear_equation.setF(float(input("Enter F:")))

    # End the script if user enters bad data (data must be numerical)
    except ValueError as err:
        print("Invalid entry! Please enter integers only.")
        exit(0)

    # If ad - bc is 0, report that “The equation has no solution.” Otherwise, print the XY results.
    if linear_equation.isSolvable():
        print("X = {:f}".format(linear_equation.getX()))
        print("Y = {:f}".format(linear_equation.getY()))
    else:
        print("The equation has no solution.")


# Make script executable as a python program
if __name__ == "__main__":
    main()
