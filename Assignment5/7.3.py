#!/usr/bin/env python

"""
7.3.py: (The Account class)

Design a class named Account that contains:
■ A private int data field named id for the account.
■ A private float data field named balance for the account.
■ A private float data field named annualInterestRate that stores the current interest rate.
■ A constructor that creates an account with the specified id (default 0), initial balance (default 100), and annual
  interest rate (default 0).
■ The accessor and mutator methods for id, balance, and annualInterestRate.
■ A method named getMonthlyInterestRate() that returns the monthly interest rate.
■ A method named getMonthlyInterest() that returns the monthly interest.
■ A method named withdraw that withdraws a specified amount from the account.
■ A method named deposit that deposits a specified amount to the account.

Draw the UML diagram for the class, and then implement the class. (Hint: The method getMonthlyInterest() is to return
the monthly interest amount, not the interest rate. Use this formula to calculate the monthly interest:
balance * monthlyInterestRate. monthlyInterestRate is annualInterestRate / 12.  Note that annualInterestRate is a
percent (like 4.5%). You need to divide it by 100.)

Write a test program that creates an Account object with an account id of 1122, a balance of $20,000, and an annual
interest rate of 4.5%. Use the withdraw method to withdraw $2,500, use the deposit method to deposit $3,000, and print
the id, balance, monthly interest rate, and monthly interest.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class Account:

    def __init__(self, id: int = 0, balance: float = 100, annualInterestRate: float = 0):
        """
        Default constructor for Account class objects
        :param id: Bank account unique identifier
        :param balance: Bank account current balance
        :param annualInterestRate: Bank account annual interest rate (percentage-based)
        """
        self.__id = id
        self.__balance = balance
        self.__annualInterestRate = annualInterestRate

    def getId(self):
        """
        Accessor function for bank account ID
        :return: bank account ID
        """
        return self.__id

    def setId(self, id: int):
        """
        Mutator function for bank account ID
        :param id: the new value for the bank account ID
        :return: Always returns True
        """
        self.__id = id
        return True

    def getBalance(self):
        """
        Access function for bank account balance
        :return: bank account balance
        """
        return self.__balance

    def setBalance(self, balance: float):
        """
        Mutator function for bank account balance
        :param balance: the new bank account balance
        :return: Always return True
        """
        self.__balance = balance
        return True

    def getAnnualInterestRate(self):
        """
        Accessor function for bank account's annual interest rate (APR)
        :return: bank account APR
        """
        return self.__annualInterestRate

    def setAnnualInterestRate(self, annual_interest_rate: float):
        """
        Mutator function for APR
        :param annual_interest_rate: the bank account's new APR value
        :return: Always return True
        """
        self.__annualInterestRate = annual_interest_rate
        return True

    def getMonthlyInterestRate(self):
        """
        Accessor function for bank account's monthly interest rate (MPR)
        :return: bank account MPR
        """
        return self.__annualInterestRate / 100 / 12

    def getMonthlyInterest(self):
        """
        Accessor function for bank account's current monthly interest accumulation
        :return: bank account's current monthly interest accumulation
        """
        return self.__balance * self.getMonthlyInterestRate()

    def withdraw(self, amount: float):
        """
        Mutator function to withdraw funds from the bank account
        :param amount: total monetary amount to deduct from the current bank account balance
        :return: the bank account balance after the withdraw transaction
        """
        self.__balance -= amount
        return amount

    def deposit(self, amount: float):
        """
        Mutator function to add funds from the bank account
        :param amount: total monetary amount to add to the current bank account balance
        :return: the bank account balance after the deposit transaction
        """
        self.__balance += amount
        return self.__balance


def main():
    # Create a mock bank account with default values: ID=1122, Balance=$20,000, APR=4.5%
    test_account = Account(1122, 20000, 4.5)

    # Withdraw $2500 from the mock bank account
    test_account.withdraw(2500)

    # Deposit $3000 into the mock bank account
    test_account.deposit(3000)

    # Print bank account information
    print("Account ID: " + str(test_account.getId()))
    print("Account Balance: " + "$" + str(test_account.getBalance()))
    print("Account Monthly Interest Rate: " + str(test_account.getMonthlyInterestRate()))
    print("Account Monthly Interest: " + "$" + str(round(test_account.getMonthlyInterest(), 2)))


# Make script executable as a python program
if __name__ == "__main__":
    main()
