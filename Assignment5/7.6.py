#!/usr/bin/env python

"""
7.6.py: (Algebra: quadratic equations)

Design a class named QuadraticEquation for a quadratic equation ax^2 + bx + x = 0. The class contains:
■ The private data fields a, b, and c that represent three coefficients.
■ A constructor for the arguments for a, b, and c.
■ Three get methods for a, b, and c.
■ A method named getDiscriminant() that returns the discriminant, which is b^2 - 4ac.
■ The methods named getRoot1() and getRoot2() for returning the two roots of the equation using these formulas:
r1 = -b + sqrt(b^2-4ac) / 2a  and  r2 = -b - sqrt(b^2-4ac) / 2a

These methods are useful only if the discriminant is nonnegative. Let these methods return 0 if the discriminant is
negative.

Draw the UML diagram for the class, and then implement the class. Write a test program that prompts the user to enter
values for a, b, and c and displays the result based on the discriminant. If the discriminant is positive, display the
two roots. If the discriminant is 0, display the one root. Otherwise, display “The equation has no roots.” See Exercise
4.1 for sample runs.
"""

# import built-in libraries
from math import sqrt
from sys import exit
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class QuadraticEquation:

    def __init__(self, a=0, b=0, c=0):
        """
        default constructor for QuadraticEquation
        :param a: a-variable in quadratic formula
        :param b: b-variable in quadratic formula
        :param c: c-variable in quadratic formula
        """
        self.__a = a
        self.__b = b
        self.__c = c

    def setA(self, a):
        """
        Mutator function for quadratic A value
        :param a: the new value for A
        :return: None
        """
        self.__a = a

    def setB(self, b):
        """
        Mutator function for quadratic B value
        :param b: the new value for B
        :return: None
        """
        self.__b = b

    def setC(self, c):
        """
        Mutator function for quadratic C value
        :param c: the new value for C
        :return: None
        """
        self.__c = c

    def getA(self):
        """
        Accessor function for quadratic A value
        :return: A value
        """
        return self.__a

    def getB(self):
        """
        Accessor function for quadratic C value
        :return: B value
        """
        return self.__b

    def getC(self):
        """
        Accessor function for quadratic C value
        :return: C value
        """
        return self.__c

    def getDiscriminant(self):
        """
        Computes the quadratic discriminant as b^2 - 4ac
        :return: quadratic discriminant calculation result
        """
        return (self.__b ** 2) - (4 * self.__a * self.__c)

    def getRoot1(self):
        """
        Computes the quadratic root1 value as -b + sqrt(b^2-4ac) / 2a
        :return: root1 calculation result
        """
        root = ((-1 * self.__b) + sqrt(self.getDiscriminant())) / 2 * self.__a
        return 0 if self.getDiscriminant() < 0 else root

    def getRoot2(self):
        """
        Computes the quadratic root2 value as -b + sqrt(b^2-4ac) / 2a
        :return: root2 calculation result
        """
        root = ((-1 * self.__b) - sqrt(self.getDiscriminant())) / 2 * self.__a
        return 0 if self.getDiscriminant() < 0 else root


def main():
    # Init an empty quadratic object
    quadratic = QuadraticEquation()

    # Try setting the quadratic variables (a, b, and c) using user input.
    try:
        quadratic.setA(int(input("Enter A:")))
        quadratic.setB(int(input("Enter B:")))
        quadratic.setC(int(input("Enter C:")))

    # End the program if user enters bad data.
    except ValueError as err:
        print("Invalid entry! Please enter integers only.")
        exit(0)

    # Calculate the quadratic discriminant
    d = quadratic.getDiscriminant()

    # If the discriminant is positive, display the two roots. If the discriminant is 0, display the one root.
    # Otherwise, display “The equation has no roots.”
    if d > 0:
        print("Root1: {:f}\nRoot2: {:f}".format(quadratic.getRoot1(), quadratic.getRoot2()))
    elif d == 0:
        print("Root1: {:f}".format(quadratic.getRoot1()))
    else:
        print("The equation has no roots.")


# Make script executable as a python program
if __name__ == "__main__":
    main()
