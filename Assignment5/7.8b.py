#!/usr/bin/env python

"""
7.8.py: (Stopwatch)

Design a class named StopWatch. The class contains:
■ The private data fields startTime and endTime with get methods.
■ A constructor that initializes startTime with the current time.
■ A method named start() that resets the startTime to the current time.
■ A method named stop() that sets the endTime to the current time.
■ A method named getElapsedTime() that returns the elapsed time for the stop watch in milliseconds.

Draw the UML diagram for the class, and then implement the class. Write a test program that measures the execution time
of adding numbers from 1 to 1,000,000.
"""

# import built-in libraries
from datetime import datetime
from datetime import MINYEAR, MAXYEAR
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class States(object):
    """
    This class represents all of the possible states of a StopWatch class object.
    """
    def __init__(self, init: bool = True, running: bool = False, stopped: bool = False):
        """
        Class constructor for the StopWatch States.
        :param init: Refers to the "initial" state of the stop watch. This is the default state.
        :param running: Refers to the state of the stop watch when it is running.
        :param stopped: Refers to the state of the stop watch after it has been stopped, but has not been reset yet.
        """
        self.init = init
        self.running = running
        self.stopped = stopped

    def start(self):
        """
        Adjusts the stop watch state values to represent a "running" stop watch.
        :return: None
        """
        self.init = False
        self.running = True
        self.stopped = False

    def stop(self):
        """
        Adjusts the stop watch state values to represent a "stopped" stop watch.
        :return: None
        """
        self.init = False
        self.running = False
        self.stopped = True

    def reset(self):
        """
        Adjusts the stop watch state values to represent a "reset" stop watch. This is the default state.
        :return: None
        """
        self.init = True
        self.running = False
        self.stopped = False


def resetDate():
    """
    Static method to reset datetime objects back to baseline values to keep code consistent
    :return: datetime object at baseline/min values
    """
    return datetime(MINYEAR, 1, 1, 0, 0, 0, 0)


def formatTime(dateTimeObject):
    """
    Static method to format datetime objects in a consistent, readable manner
    :param dateTimeObject: The datetime object whose attributes should be formatted
    :return: formatted datetime object in the formatL [H(hours), M(minutes) S(seconds), f(microseconds)]
    """
    TIME_FORMAT = "%H(hr) %M(min) %S(sec) %f(ms)"
    return dateTimeObject.strftime(TIME_FORMAT)


class StopWatch:

    def __init__(self, startTime=resetDate(), endTime=resetDate(),
                 timeElapsed=resetDate(), state=States(init=True)):
        """
        Default constructor for StopWatch class objects
        :param startTime: datetime object that represents the starting time
        :param endTime: datetime object that represents the ending time
        :param timeElapsed: datetime object that represents the sum of time elapsed between start and end, since the
                            last reset occurrence.
        :param state: Represents the current state of the stop watch (either init, running, or stopped)
        """
        self.__startTime = startTime
        self.__endTime = endTime
        self.__state = state
        self.__timeElapsed = timeElapsed

    def getCurrentState(self):
        """
        Accessor method for the current state of the stop watch
        :return: A State object that contains booleans for each of the three possible state values: init, running,
                 or stopped
        """
        return self.__state

    def getStartTime(self):
        """
        Accessor method for the stop watch's start time
        :return: start time
        """
        # Only return the start time if the stop watch is either running, or has been stopped.
        if self.__state.running or self.__state.stopped:
            return formatTime(self.__startTime)
        # Do not return the start time if the stop watch has not been started yet.
        if self.__state.init:
            return "Not allowed in current state."

    def getEndTime(self):
        """
        Accessor method for the stop watch's end time
        :return: end time
        """
        # Do not return the end time if the stop watch has not been started (init) or if the stop watch is still running
        if self.__state.init or self.__state.running:
            return "Not allowed in current state."
        # Only return the end time if the stop watch is stopped.
        if self.__state.stopped:
            return formatTime(self.__endTime)

    def start(self):
        """
        Mutator method that handles starting the stop watch.
        :return: Returns the start time under legal circumstances, otherwise returns a str object indicating that the
                 method call was not allowed.
        """
        if self.__state.init:
            # Start the timer: Capture the start time as the moment the start() method was called, and set the correct
            # timer state
            self.__startTime = datetime.now()
            self.__state.start()
            return self.__startTime

        if self.__state.running:
            # Cannot start a timer that is already running. Do nothing.
            return "Not allowed in current state."

        if self.__state.stopped:
            # Start the timer which is currently paused. Reset the start time and end time to matching values, which we
            # we consider to be the time at which the start() method was called while the timer was paused.
            self.__startTime = datetime.now()
            self.__endTime = self.__startTime
            self.__state.start()
            return self.__startTime

    def stop(self):
        """
        Mutator method that handles stopping the stop watch.
        :return: Returns the end time at which the stop watch was paused, if the stop watch was paused under legal
                 circumstances. Otherwise returns a str object indicating that the method call was not allowed.
        """
        if self.__state.init:
            # Cannot stop the timer when state is in initial state. Do nothing.
            return "Not allowed in current state."

        if self.__state.running:
            # Stop the running timer: Capture the end time as the moment the stop() method was called, and calculate
            # the time elapsed.

            # Capture the end time
            self.__endTime = datetime.now()

            # Calculate the time elapsed since the last start() method invocation
            time_delta = self.__endTime - self.__startTime

            # Append the calculated time elapsed to the existing time elapsed. Continue until the timer is reset.
            self.__timeElapsed += time_delta

            # Set the new state
            self.__state.stop()

            return self.__endTime

        if self.__state.stopped:
            # Cannot stop the timer when it is already stopped. Do nothing.
            return "Not allowed in current state."

    def reset(self):
        """
        Mutator method that resets the stop watch back to initial state.
        :return: Returns nothing if the reset method completed successfully. Otherwise returns a str object indicating
                 that the method was invoked under illegal circumstances.
        """
        # Not allowed to reset the stop watch if it is already at initial state, or if it is actively running.
        if self.__state.init or self.__state.running:
            # Cannot reset a timer that has not started.
            # By design, will not reset a timer that is actively running.
            return "Not allowed in current state."

        # Only reset the stop watch if the stop watch is already paused.
        if self.__state.stopped:
            self.__state.reset()
            self.__timeElapsed = resetDate()
            self.__startTime = resetDate()
            self.__endTime = resetDate()

    def getElapsedTime(self):
        """
        Accessor method for the time elapsed
        :return: the time elapsed since the last reset invocation. (The time elapsed will stack on previous start/stop
                 calls).
        """
        # Only return the elapsed tme if the stop watch is currently stopped, or if it is running. Calling it while
        # running will only show the last *recorded* time elapsed. The value is only updated when the stop watch is
        # stopped.
        if self.__state.stopped or self.__state.running:
            return formatTime(self.__timeElapsed)
        # Not allowed to get elapsed time when the stop watch is at initial state.
        if self.__state.init:
            return "Not allowed in current state."


def killSomeTime():
    """
    Static method to allow the user to actively let the timer run. The method indefinitely loops until the user enters
    any key.
    :return: None
    """
    while True:
        i = input("Enter anything to stop timer: ")
        if not i:
            break


def printResults(stopWatchObject):
    """
    Prints the start time, end time, and the time elapsed recorded on the given stop watch. Note that the start time and
    end time only refers to the *last recorded* instances of start and end. They may be overwritten by subsequent start/
    end calls, although the time elapsed will continue to increment until the reset() method is invoked.
    :param stopWatchObject: The StopWatch object that should be analyzed.
    :return: None
    """
    diff = stopWatchObject.getElapsedTime()
    # Print results
    print("The start time was   : {}".format(stopWatchObject.getStartTime()))
    print("The end time was     : {}".format(stopWatchObject.getEndTime()))
    print("The time elapsed was : {}".format(stopWatchObject.getElapsedTime()))
    # print("THe time elapsed was : {:f}(sec) {:f}(ms)".format(diff.second, diff.microsecond / 1000))


def test1():
    print("\nTEST 1: BASIC START/STOP FUNCTIONALITY")
    # Instantiate a stop watch object
    stop_watch = StopWatch()

    # Start the stop watch
    stop_watch.start()

    # Kill some time
    killSomeTime()

    # Stop the stop watch
    stop_watch.stop()

    printResults(stop_watch)


def test2():
    print("\nTEST 2: CONTINUE TIMER FROM PREVIOUS STATE")
    # Instantiate a stop watch object
    stop_watch = StopWatch()

    # Start the stop watch
    stop_watch.start()

    # Kill some time
    killSomeTime()

    # Stop the stop watch
    stop_watch.stop()

    # Without resetting the timer, start it again (where it left off)
    stop_watch.start()

    # Kill some more time
    killSomeTime()

    # Stop the stop watch
    stop_watch.stop()

    printResults(stop_watch)


def test3():
    print("\nTEST 3: DEMO HANDLERS FOR ILLEGAL CALLS")
    stop_watch = StopWatch()
    stop_watch.start()
    killSomeTime()
    print(stop_watch.getElapsedTime(), "<-- Notice the elapsed time is zero no matter how long you wait, b/c the timer "
                                       "has not been stopped yet!")
    killSomeTime()
    stop_watch.stop()
    time_a = stop_watch.getElapsedTime()
    print(time_a, "<-- This time we actually stopped the timer before fetching the time elapsed.")

    stop_watch.start()
    killSomeTime()
    stop_watch.stop()
    time_b = stop_watch.getElapsedTime()
    print(time_b, "<-- This timer appended your new wait duration to the last wait duration b/c you didn't reset the "
                  "timer first!")

    stop_watch.reset()
    killSomeTime()
    time_c = stop_watch.getElapsedTime()
    print(time_c, "<-- This time we reset the clock back to 'init' then tried to fetch the time elapsed")

    stop_watch.start()
    killSomeTime()
    stop_watch.stop()
    time_d = stop_watch.getElapsedTime()
    print(time_d, "<-- This time we remembered to start the timer, then stop it! So we have a fresh time lapse here!")


def main():
    test1()
    test2()
    test3()


# Make script executable as a python program
if __name__ == "__main__":
    main()
