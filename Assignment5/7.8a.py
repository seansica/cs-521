#!/usr/bin/env python

"""
7.8.py: (Stopwatch)

Design a class named StopWatch. The class contains:
■ The private data fields startTime and endTime with get methods.
■ A constructor that initializes startTime with the current time.
■ A method named start() that resets the startTime to the current time.
■ A method named stop() that sets the endTime to the current time.
■ A method named getElapsedTime() that returns the elapsed time for the stop watch in milliseconds.

Draw the UML diagram for the class, and then implement the class. Write a test program that measures the execution time
of adding numbers from 1 to 1,000,000.
"""

# import built-in libraries
from datetime import datetime
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class StopWatch:

    def __init__(self, startTime=0, endTime=0):
        """
        Default constructor for StopWatch class objects
        :param startTime: datetime object that represents the starting time
        :param endTime: datetime object that represents the ending time
        """
        self.__startTime = startTime
        self.__endTime = endTime

    def getStartTime(self):
        """
        Accessor method for the stop watch's start time
        :return: start time
        """
        # print("start ", self.__startTime)
        return self.__startTime

    def getEndTime(self):
        """
        Accessor method for the stop watch's end time
        :return: end time
        """
        # print("end ", self.__endTime)
        return self.__endTime

    def start(self):
        """
        Sets the start time to the current system time down to the microsecond
        :return: the new start time
        """
        self.__startTime = datetime.now()
        return self.__startTime

    def stop(self):
        """
        Sets the end time to the current stem time down to the microsecond
        :return: the new end time
        """
        self.__endTime = datetime.now()
        return self.__endTime

    def getElapsedTime(self):
        """
        Computes the timedelta between the startTime and endTime
        :return: a timedelta object representing the amount of time passed between the start and end times
        """
        return self.__endTime - self.__startTime


def main():
    # Instantiate a stop watch object
    stop_watch = StopWatch()

    # Start the stop watch
    stop_watch.start()

    # Add all ints from 1 to 1,000,000 to kill some time
    for i in range(1, 1000000):
        i += i+1
        # print(i)

    # Stop the stop watch
    stop_watch.stop()

    # Calculate the time delta
    diff = stop_watch.getElapsedTime()

    # Print results
    print("Start Time  : {}".format(stop_watch.getStartTime()))
    print("End Time    : {}".format(stop_watch.getEndTime()))
    print("TIME DIFFERENCE:")
    print("Seconds     : {:f}\n"
          "Milliseconds: {:f}".format(diff.seconds, diff.microseconds/1000))


# Make script executable as a python program
if __name__ == "__main__":
    main()
