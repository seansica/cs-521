#!/usr/bin/env python

"""
7.1.py: (The Rectangle class)

Following the example of the Circle class in Section 7.2, design a class named Rectangle to represent a rectangle. The
class contains:
- Two data fields named width and height.
- A constructor that creates a rectangle with the specified width and height. The default values are 1 and 2 for the
  width and height, respectively.
- A method named getArea() that returns the area of this rectangle.
- A method named getPerimeter() that returns the perimeter.

Draw the UML diagram for the class, and then implement the class. Write a test program that creates two Rectangle
objects — one with width 4 and height 40 and the other with width 3.5 and height 35.7. Display the width, height, area,
and perimeter of each rectangle in this order.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class Rectangle:

    def __init__(self, width=1.0, height=2.0):
        """
        Default constructor for Rectangle class objects
        :param _width: numerical width dimension of the rectangle
        :param _height: numerical height dimension of the rectangle
        """
        self.__width = width
        self.__height = height

    def get_area(self):
        """
        Computes the area of the rectangle
        :return: area of the rectangle
        """
        return self.__height * self.__width

    def get_perimeter(self):
        """
        Computes the perimeter of the rectangle
        :return: perimeter of the rectangle
        """
        return 2 * self.get_area()

    def get_width(self):
        """
        Accessor function for the numerical width dimension
        :return: width dimension
        """
        return self.__width

    def get_height(self):
        """
        Accessor function for the numerical height dimension
        :return: height dimension
        """
        return self.__height


def main():

    # Create two rectangles
    rectangle1 = Rectangle(4, 10)
    rectangle2 = Rectangle(3.5, 35.7)

    # Add them to a list for ease of access
    rectangles = [rectangle1, rectangle2]

    # Iterate over the rectangles and print their calculated dimensions
    i = 1
    for r in rectangles:
        print("Name : " + "Rectangle" + str(i))
        print("Width : " + str(r.get_width()))
        print("Height : " + str(r.get_height()))
        print("Area : " + str(r.get_area()))
        print("Perimeter : " + str(r.get_perimeter()) + "\n")
        i += 1


# Make script executable as a python program
if __name__ == "__main__":
    main()
