#!/usr/bin/env python

"""
5.42.py: (Monte Carlo simulation)

A square is divided into four smaller regions as shown in (a). If you throw a dart into the square one million times,
what is the probability for the dart to fall into an odd-numbered region? Write a program to simulate the process and
display the result.
(Hint: Place the center of the square in the center of a coordinate system, as shown in (b). Randomly generate a point
in the square and count the number of times for a point to fall in an odd-numbered region.)
"""
import sys
import random

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


DART_COUNT = 1000000

# generate a random number to use as the absolute boundaries of the square
RANDOM_SQUARE_BOUNDS = random.uniform(-1 * sys.maxsize, sys.maxsize)


def main():

    odd_hits = 0
    even_hits = 0

    for dart in range(DART_COUNT):

        # generate square size using random number seed
        if RANDOM_SQUARE_BOUNDS <= 0:
            x_coordinate = random.uniform((-1 * RANDOM_SQUARE_BOUNDS), RANDOM_SQUARE_BOUNDS)
            y_coordinate = random.uniform((-1 * RANDOM_SQUARE_BOUNDS), RANDOM_SQUARE_BOUNDS)
        else:
            x_coordinate = random.uniform(RANDOM_SQUARE_BOUNDS, (-1 * RANDOM_SQUARE_BOUNDS))
            y_coordinate = random.uniform(RANDOM_SQUARE_BOUNDS, (-1 * RANDOM_SQUARE_BOUNDS))

        data_point = "(" + str(x_coordinate) + "," + str(y_coordinate) + ")"

        if x_coordinate <= 0:
            # hit region must be 1
            print(1, data_point)
            odd_hits += 1

        elif x_coordinate >= 0 and y_coordinate <= 0:
            # hit region must be 4
            print(4, data_point)
            even_hits += 1

        else:
            # hit region must be either 2 or 3

            # calculate the maximum x-intercept
            reference_x_intercept = RANDOM_SQUARE_BOUNDS/(-1 * RANDOM_SQUARE_BOUNDS)

            # calculate the x-intercept of the random point
            random_x_intercept = y_coordinate / (-1 * x_coordinate)

            # if the x-intercept is less than the max intercept, then the matched region is 3/odd, otherwise it's 2/even
            if random_x_intercept < reference_x_intercept:
                print(3, data_point)
                odd_hits += 1
            else:
                print(2, data_point)
                even_hits += 1

    # print the stats
    print("EVEN HITS=", even_hits)
    print("ODD HITS=", odd_hits)
    ratio = round(odd_hits / (odd_hits + even_hits) * 100, 2)
    print("ODD HIT RATIO= " + str(ratio) + "%")


# Make script executable as a python program
if __name__ == "__main__":
    main()
