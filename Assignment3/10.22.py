#!/usr/bin/env python

"""
10.22.py: (Simulation: coupon collector’s problem)

Coupon Collector is a classic statistics problem with many practical applications. The problem is to pick objects from
a set of objects repeatedly and find out how many picks are needed for all the objects to be picked at least once. A
variation of the problem is to pick cards from a shuffled deck of 52 cards repeatedly and find out how many picks are
needed before you see one of each suit. Assume a picked card is placed back in the deck before picking another. Write a
program to simulate the number of picks needed to get four cards, one from each suit and display the four cards picked
(it is possible a card may be picked twice).
"""

# import built-in libraries
import itertools
import random
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def main():
    # Initialize a deck of cards
    deck = list(itertools.product(range(1, 14), ['Spade', 'Heart', 'Diamond', 'Club']))

    # Keep track of all drawn cards. We need this in order to print them at the end.
    card_picks = []

    # Track whether one of each suit has been pulled.
    suit_count = {"Spade": 0, "Heart": 0, "Diamond": 0, "Club": 0}

    # Track the total number of cards picked throughout the simulation
    total_picks = 0

    reset_counter = 0

    # Continue pulling cards until the four latest drawn cards are all of a different suit.
    while any(count == 0 for suit, count in suit_count.items()):

        # Reset count to zero every four card pulls
        if reset_counter == 4:

            # Reset count for all suits back to zero
            for k, v in suit_count.items():
                suit_count[k] = 0

            # Reset counter back to zero
            reset_counter = 0
            # print("\nRESET DONE! -->", suit_count, "\n")

        # First, shuffle the deck of cards
        random.shuffle(deck)

        # Next, pull a card from top of the deck
        pulled_card = deck[0]
        card_picks.append(pulled_card)
        # print("PULLED CARD:", pulled_card)

        # Increment the total picks by one
        total_picks += 1

        # Record the pulled card under the suit_count tracker
        suit_count[pulled_card[1]] += 1
        # print("SUIT COUNT=", suit_count)

        reset_counter += 1

        # At this point, the while loop will end if at least one of every suit has been pulled.
        # Otherwise, it will reset the suit count and restart.
        # The total count is persistent for all iterations and will be returned after criteria met.

    # Print the last four drawn cards
    for card in card_picks[-4:]:
        print(card)

    # Print the total number of pulls that occurred before four cards of different suits were pulled
    print("Number of picks:", total_picks)


# Make script executable as a python program
if __name__ == "__main__":
    main()
