#!/usr/bin/env python

"""
5.46.py: (Statistics: compute mean and standard deviation)

In business applications, you are often asked to compute the mean and standard deviation of data. The mean is simply
the average of the numbers. The standard deviation is a statistic that tells you how tightly all the various data are
clustered around the mean in a set of data. For example, what is the average age of the students in a class? How close
are the ages? If all the students are the same age, the deviation is 0. Write a program that prompts the user to enter
ten numbers, and displays the mean and standard deviations of these numbers using the following formula

mean = (n1 + n2 + n3 + n4 + n5) / 5

standard deviation = (see image in textbook)

Addendum:
Instead of having user enter 10 numbers, Your program should let user enter any number of numbers, and then display
output upon user hitting <enter> without any number. (Please note that the sample output in the textbook in incorrect.
Given those input numbers, the mean should be 5.71, and the standard deviation should be 2.97)
"""

# import built-in libraries
import math
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def calculate_mean(integers: list):
    """
    Calculate the mean value of a list of integers.
    :param integers: A list of integers from which the mean will derive.
    :return: Mean value calculation
    """
    # mean = (n1 + n2 + n3 + n4 + n5 + ... + nx) / x, where x equals the total number of integers in the list
    return sum(integers) / len(integers)


def calculate_standard_deviation(integers: list):
    """
    Calculate the standard deviation of a list of integers.
    :param integers: A list of integers from which the standard deviation will derive.
    :return: Standard deviation calculation
    """
    # Initialize placeholder container for each integers variance from the mean value
    variance = []

    for i in integers:

        # 1. Determine how far each integer varies from the mean of the collective list of integers
        # 2. Subtract the variance from each integer
        # 3. Square the result of each mean variance value
        variance_from_mean = (i - calculate_mean(integers)) ** 2
        variance.append(variance_from_mean)

    # 4. Sum all of the values calculated from Step 4
    # 5. Divide the sum calculated in Step 4 by (n-1) where n equals the total number of integers in the list.
    #    This returns the variance.
    # 6. Square root the variance value calculated in Step 5 to produce the standard deviation
    return math.sqrt(sum(variance) / (len(integers) - 1))


def isReal(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def get_integers():
    """
    Prompts the user to input data into the console. Only accepts numeric integers. Immediately breaks and returns the
    list of integers if user enters non-numeric characters, including empty lines.
    :return: List of integers (formatted as floats to accommodate rational numbers)
    """
    # Initialize placeholder container for user-defined numeric values
    integers = []

    # Initialize infinite loop
    while True:

        # Record user input
        user_input = input("Enter any integer [blank newline to escape]:")

        # Determine whether user input is a Real number (includes all float values such as whole and rational numbers)
        if isReal(user_input):

            # Match success! User input converted to float and added to list
            integers.append(float(user_input))

        # Match failed. User assumed to have finished recording. Break loop and return list of integers.
        else:
            return integers


def main():
    """
    Main function runs upon program execution. It's purpose is to record integers from user input and determine the
    mean and standard deviation of the collection of user-defined integers.
    :return:
    """
    integers = get_integers()
    print(integers)
    print("The mean is", calculate_mean(integers))
    print("The standard deviation is", calculate_standard_deviation(integers))


# Make script executable as a python program
if __name__ == "__main__":
    main()
