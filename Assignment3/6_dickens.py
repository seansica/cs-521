#!/usr/bin/env python

"""
6_dickens.py:
Count the frequency distribution for each alphabetical character in the input file “sample_input_charles_dickens.txt”
(first three paragraphs from Chapter 1 in Charles Dicken’s novel “Great Expectations”). Write the result into a well
formatted file (you may use \t as separator). Ignore cases and non-alphabetic characters.

Here is a sample output:

Char    Freq    %total
A       172     9.55
B        33     1.83
C        39     2.17
D        82     4.55
E       217    12.05
F        52     2.89
G        41     2.28
H       108     6.00
I       134     7.44
J         1     0.06
K        14     0.78
L        65     3.61
M        53     2.94
N       122     6.77
O       112     6.22
P        28     1.55
Q         1     0.06
R       128     7.11
S        99     5.50
T       160     8.88
U        29     1.61
V        24     1.33
W        44     2.44
X         4     0.22
Y        38     2.11
Z         1     0.06
"""

import collections

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

INPUT_FILENAME = "sample_input_charles_dickens.txt"
OUTPUT_FILENAME = "dickens_out.txt"


def main():
    # Initialize a counter to analyze the input file
    c = collections.Counter()
    # Open the input file
    with open(INPUT_FILENAME) as file:
        # Iterate over each line of the file
        for line in file:
            # Covert all characters to uppercase for each of lower/upper tracking
            line = line.upper()
            # Iterate over each character on each line
            for character in line:
                # Only match alphanumeric characters; skip numbers and special characters
                if character.isalpha():
                    # Add the character to the collection. Keeps track of duplicate entries.
                    c += collections.Counter(character)

    # Add all of the alphanumeric character counts together in order to determine the distribution percentage of each
    total_count = sum(c.values())

    # Sort the collection in alphabetic order
    sorted_collection = dict(sorted(c.items(), key=lambda item: item[0]))

    # Open/write to the output file
    with open(OUTPUT_FILENAME, 'w') as file:
        # Iterate over the sorted dictionary of character counts
        for letter, count in sorted_collection.items():
            # Determine the distribution of each character as the ratio of each count to the total count
            dist = str(round(100 * (count / total_count), 2)) + "%"
            # Build a line to write out for each character in the collection.
            line = str("Letter: " + letter + "\t\tCount: " + str(count) + "\t\t%Total: " + dist + "\n")
            # Write each alphanumeric char distribution info to the output file
            file.write(line)


# Make script executable as a python program
if __name__ == "__main__":
    main()
