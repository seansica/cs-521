#!/usr/bin/env python

"""
13.4.py: (Write/read data)

Write a program that writes 100 integers created randomly into a file. Integers are separated by a space in the file.
Read the data back from the file and display the sorted data. Your program should prompt the user to enter a file-name.
If the file already exists, do not override it.)

The sentence "If the file already exists, do not override it" should really be "If the file already exists, do not
overwrite it".
"""


import random
import os
import csv


__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

COUNT = 100  # Represents the number of random numbers that should be generated and written to the file
FILENAME = input("Enter a filename:")  # Prompt user for filename to write to


def main():
    # Generate placeholder for random unsorted numbers
    random_integers = []

    try:
        # Raise an error if the filename already exists
        if os.path.exists(FILENAME):
            raise FileExistsError("The file already exists")

        else:
            # Generate random numbers
            for r in range(COUNT):
                num = str(random.randint(0, 9999))
                # Add the random number to the random number list
                random_integers.append(num)

            # Write the numbers to file
            write_unsorted_file(random_integers)

        # Sort and print the numbers
        read_sorted_file()

    except FileExistsError as error:
        print(repr(error))


def write_unsorted_file(list_of_stuff: list):
    """
    Creates a new file and writes a list of elements to the file
    :param list_of_stuff: List of elements to write to the file
    """
    # Open the file in write mode
    file = open(FILENAME, 'w')

    # Generate a counter to determine when we have reached the end of the list of stuff
    ctr = 0

    # Loop over the list of stuff
    for element in list_of_stuff:

        # If we have reached the end of the list
        if ctr == len(list_of_stuff) - 1:
            # ...write the last element
            file.write(element)
        else:
            # ...otherwise write the element and a whitespace char (" ") for delimiting elements
            file.write(element + " ")

        # Increment the counter
        ctr += 1

    # Close the file
    file.close()


def read_sorted_file():
    """
    Opens the file and prints all integer elements to the console in numerically sorted order
    """
    # Placeholder container to store all integers
    unsorted_numbers = []

    # Open the file
    with open(FILENAME, newline='') as file:

        # Use csv.reader to loop over all rows of the file (even though it will be 1 always, still good to do)
        reader = csv.reader(file, delimiter=' ')
        for row in reader:
            # Join all rows into one monolithic string
            file_contents = ' '.join(row)
            # Break up the monolithic string in whitespace-separated list
            unsorted_numbers = file_contents.split(' ')
            # Convert all of the elements into integers for sorting purposes
            unsorted_numbers = list(map(int, unsorted_numbers))

    # Sort and print the integers to the console
    [print(str(i)) for i in sorted(unsorted_numbers)]


# Make script executable as a python program
if __name__ == "__main__":
    main()
