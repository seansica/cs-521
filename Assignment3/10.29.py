#!/usr/bin/env python

"""
10.29.py: (Game: hangman)

Write a hangman game that randomly generates a word and prompts the user to guess one letter at a time, as shown in the
sample run. Each letter in the word is displayed as an asterisk. When the user makes a correct guess, the actual letter
is then displayed. When the user finishes a word, dis- play the number of misses and ask the user whether to continue
playing. Create a list to store the words, as follows:

# Use any words you wish
words = ["write", "that", "program", ...]
"""

# import built-in libraries
import random

# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

RANDOM_WORD = ["Python", "Java", "Django", "Ansible", "Programming", "Docker", "Cisco", "Chicken", "Noodle", "Avocado"]


def list_to_string(list_of_chars: list):
    """
    Simple function to convert a list of chars to a string for console readability
    :param list_of_chars: Concatenate all chars into a str
    :return: A single str
    """
    word = ""
    for elem in list_of_chars:
        word += elem
    return str(word)


def reset_game():
    """
    Sets the required game variables, including the guess miss tracker, the random word, and the
    "hidden suitcase" word tracker
    :return: A random string, x, a list of asterisks of length x, and a counter set to zero
    """
    # Fetch a random word from the list of available words
    random_word = random.choice(RANDOM_WORD)

    # Generate a list of asterisks; one asterisk per character in random_word
    word_tracker = ["*"] * len(random_word)

    # Generate a guess tracker
    misses = 0
    return random_word, word_tracker, misses


def main():
    """
    Play hangman!
    """
    # Generate a word and set the default game variables
    random_word, word_tracker, misses = reset_game()

    # Start the game!
    while True:

        if "*" not in word_tracker:
            # Game Over!

            # Print the results
            print("The word is", list_to_string(word_tracker) + ".", "You missed", misses, "time")

            # Reset the game
            play_again = input("Do you want to play again?[y/n] > ")
            if play_again == "y":
                random_word, word_tracker, misses = reset_game()
            else:
                print("Game Over!")
                break

        # Prompt the user for a guess
        prompt = "(Guess) Enter a letter in word " + list_to_string(word_tracker) + " > "
        user_input = input(prompt)

        # By design, only accept the first character of whatever input that the user entered
        guess = user_input[0]

        # Provide a means of breaking the infinite loop
        if guess == "!":
            print("Game Over!")
            break

        # Determine if the guess was a match
        if guess in random_word:

            # Match found! Determine if the user has already guessed this word
            if guess not in word_tracker:

                # New match!
                # Get a list of all indices that match the user's guess
                indices = [i for i, x in enumerate(random_word) if x == guess]

                # Reveal the hidden letter(s) that the user correctly guessed
                for i in indices:
                    word_tracker[i] = guess
                    print(word_tracker)

            # Match already made. Increment miss count.
            else:
                misses += 1
                print(guess, "is already in the word")

        # Bad guess. Increment miss count.
        else:
            misses += 1
            print(guess, "is not in the word")


# Make script executable as a python program
if __name__ == "__main__":
    main()
