#!/usr/bin/env python

"""
15.3.py: (Compute greatest common divisor using recursion)

The gcd(m, n) can also be defined recursively as follows:

- If m % n is 0, gcd(m, n) is n.
- Otherwise, gcd(m, n) is gcd(n, m % n).

Write a recursive function to find the GCD. Write a test program that prompts the user to enter two integers and
displays their GCD.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def gcd(m, n):
    if m % n == 0:
        return n

    return gcd(m, (m % n))


def main():
    testM = int(input("Enter an integer m: "))
    testN = int(input("Enter an integer n: "))
    print("The GCD of {} and {} is {}".format(testM, testN, gcd(testM, testN)))


# Make script executable as a python program
if __name__ == "__main__":
    main()
