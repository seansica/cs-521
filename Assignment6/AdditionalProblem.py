#!/usr/bin/env python

"""
AdditionalProblem.py:

Define an exception class for Account in 7.3. Update the Account class to have it raise your user-defined exception when
the following arguments are negative.

- annualInterestRate passed to __init__()
- amount passed to withdraw()
- amount passed to deposit()
- Update the test function to handle the exception.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class IllegalBankAction(Exception):
    """Raised when illegal values passed to Account functions"""
    pass


class Account:

    def __init__(self, id: int = 0, balance: float = 100, annualInterestRate: float = 0):
        """
        Default constructor for Account class objects
        :param id: Bank account unique identifier
        :param balance: Bank account current balance
        :param annualInterestRate: Bank account annual interest rate (percentage-based)
        """
        if annualInterestRate < 0:
            raise IllegalBankAction("Annual Interest Rate cannot be negative.")
        self.__id = id
        self.__balance = balance
        self.__annualInterestRate = annualInterestRate

    def getId(self):
        """
        Accessor function for bank account ID
        :return: bank account ID
        """
        return self.__id

    def setId(self, id: int):
        """
        Mutator function for bank account ID
        :param id: the new value for the bank account ID
        :return: Always returns True
        """
        self.__id = id
        return True

    def getBalance(self):
        """
        Access function for bank account balance
        :return: bank account balance
        """
        return self.__balance

    def setBalance(self, balance: float):
        """
        Mutator function for bank account balance
        :param balance: the new bank account balance
        :return: Always return True
        """
        self.__balance = balance
        return True

    def getAnnualInterestRate(self):
        """
        Accessor function for bank account's annual interest rate (APR)
        :return: bank account APR
        """
        return self.__annualInterestRate

    def setAnnualInterestRate(self, annual_interest_rate: float):
        """
        Mutator function for APR
        :param annual_interest_rate: the bank account's new APR value
        :return: Always return True
        """
        self.__annualInterestRate = annual_interest_rate
        return True

    def getMonthlyInterestRate(self):
        """
        Accessor function for bank account's monthly interest rate (MPR)
        :return: bank account MPR
        """
        return self.__annualInterestRate / 100 / 12

    def getMonthlyInterest(self):
        """
        Accessor function for bank account's current monthly interest accumulation
        :return: bank account's current monthly interest accumulation
        """
        return self.__balance * self.getMonthlyInterestRate()

    def withdraw(self, amount: float):
        """
        Mutator function to withdraw funds from the bank account
        :param amount: total monetary amount to deduct from the current bank account balance
        :return: the bank account balance after the withdraw transaction
        """
        if amount < 0:
            raise IllegalBankAction("Cannot withdraw negative amount.")
        self.__balance -= amount
        return amount

    def deposit(self, amount: float):
        """
        Mutator function to add funds from the bank account
        :param amount: total monetary amount to add to the current bank account balance
        :return: the bank account balance after the deposit transaction
        """
        if amount < 0:
            raise IllegalBankAction("Cannot deposit negative amount.")
        self.__balance += amount
        return self.__balance


def main():
    # Test negative APR value passed to constructor
    try:
        # Create a mock bank account with default values: ID=1122, Balance=$20,000, APR= -4.5%
        test_account = Account(1122, 20000, -4.5)

    except IllegalBankAction as err:
        print(repr(err))

    # Test negative param passed to deposit function
    try:
        # Create a mock bank account with default values: ID=1122, Balance=$20,000, APR=4.5%
        test_account = Account(1122, 20000, 4.5)

        # Withdraw -$3000 from mock bank account
        test_account.withdraw(-3000)

    except IllegalBankAction as err:
        print(repr(err))

    # Test negative param passed to deposit function
    try:
        # Create a mock bank account with default values: ID=1122, Balance=$20,000, APR=4.5%
        test_account = Account(1122, 20000, 4.5)

        # Deposit -$3000 into the mock bank account
        test_account.deposit(-3000)

    except IllegalBankAction as err:
        print(repr(err))


# Make script executable as a python program
if __name__ == "__main__":
    main()
