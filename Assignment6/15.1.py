#!/usr/bin/env python

"""
15.1.py: (Sum the digits in an integer using recursion)

Write a recursive function that computes the sum of the digits in an integer. Use the following function header:

def sumDigits(n):

For example, sumDigits(234) returns 2 + 3 + 4 = 9. Write a test program that prompts the user to enter an integer and
displays its sum.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def sumDigits(number):
    if number == 0:
        return 0
    else:
        # sumDigits(234)
        #   234 % 10 == 4 -----> ADD THIS
        #   234 // 10 == 23
        # sumDigits(23)
        #   23 % 10 == 3 ------> ADD THIS
        #   23 // 10 == 2
        # sumDigits(2)
        #   2 % 10 == 2 -------> ADD THIS
        #   2 // 10 == 0
        # sumDigits(0)
        #   2 % 10 == 0 -------> ADD THIS
        return (number % 10) + sumDigits(number // 10)


def main():
    myNumber = int(input("Enter an integer: "))
    total = sumDigits(myNumber)
    print(total)


# Make script executable as a python program
if __name__ == "__main__":
    main()
