#!/usr/bin/env python

"""
15.23.py: (String permutation)

Write a recursive function to print all the permutations of a string. For example, for the string abc, the printout is:
  abc
  acb
  bac
  bca
  cab
  cba
(Hint: Define the following two functions. The second function is a helper function.

def displayPermuation(s):
def displayPermuationHelper(s1, s2):

The first function simply invokes displayPermuation(" ", s). The second function uses a loop to move a character from
s2 to s1 and recursively invokes it with a new s1 and s2. The  base case is that s2 is empty and prints s1 to the
console.)

Write a test program that prompts the user to enter a string and displays all its permutations.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def displayPermutations(s):
    # Base case
    if len(s) == 1:
        return [s]
    # Container for all permutations
    result = []
    # enumerate to access index and value simultaneously
    # index will be used for string slicing
    for i, v in enumerate(s):
        result += [v + p for p in displayPermutations(s[:i] + s[i + 1:])]
    # done
    return result


def main():
    userInput = input("Enter a string: ")
    print('\n'.join(displayPermutations(userInput)))
    return


# Make script executable as a python program
if __name__ == "__main__":
    main()
