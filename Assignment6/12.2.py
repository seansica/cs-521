#!/usr/bin/env python

"""
12.2.py: (The Location class) Design a class named Location for locating a maximal value and its location in a
two-dimensional list. The class contains the public data fields row, column, and maxValue that store the maximal value
and its indexes in a two-dimensional list, with row and column as int types and maxValue as a float type.

Write the following method that returns the location of the largest element in a two-dimensional list.
def Location locateLargest(a):
The return value is an instance of Location. Write a test program that prompts the user to enter a two-dimensional list
and displays the location of the largest ele- ment in the list.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def locateLargest(twoDArray):
    # Init Location placeholder object
    largest = Location()

    # Iterate over the matrix
    for i in range(len(twoDArray)):
        for j in range(len(twoDArray[i])):
            # If currently element in iteration is largest than the current largest, set the currently selected element
            # as the new largest and update the Location object to reflect the coordinates and value of that element.
            if twoDArray[i][j] > twoDArray[largest.row][largest.column]:
                largest.row = i
                largest.column = j
                largest.maxValue = twoDArray[i][j]

    return largest


class Location:
    def __init__(self, row: int = 0, column: int = 0, maxValue: float = 0):
        self.row = row
        self.column = column
        self.maxValue = maxValue
        pass


def main():
    # Prompt user to enter number of rows and columns to instantiate
    userInput = input("Enter the number of rows and columns in the list:")

    # Parse out the user-entered row/column values
    numberOfRows, numberOfColumns = [int(values.strip()) for values in userInput.split(',')]

    # Create placeholder 2d-array
    twoDimensionalArray = []

    # Loop over each row and fill them out with integer values
    for index, element in enumerate(range(numberOfRows)):
        # Prompt user to enter row element
        userInput = input("Enter row " + str(index) + ":")

        # Add the user-defined element to the row
        row = [float(xValue) for xValue in userInput.split(" ")]

        # Raise an error if user enters too few or too many elements in a row
        if len(row) != numberOfColumns:
            raise SystemError("Number of values entered does not match expected amount. " +
                              str(len(row)) + " detected but " + str(numberOfColumns) + " expected!")

        # If user enters correct number of elements, complete the row and move onto the next
        twoDimensionalArray.append(row)

    # Find the largest element in the matrix
    largest = locateLargest(twoDimensionalArray)

    # Print it out
    print("The location of the largest element is {} at ({}, {})".format(largest.maxValue, largest.row, largest.column))


# Make script executable as a python program
if __name__ == "__main__":
    main()
