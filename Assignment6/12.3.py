#!/usr/bin/env python

"""
12.3.py: (Game: ATM machine)

Use the Account class created in Exercise 7.3 to simulate an ATM machine. Create ten accounts in a list with the ids
0, 1, ..., 9, and an initial balance of $100. The system prompts the user to enter an id. If the id is entered
incorrectly, ask the user to enter a correct id. Once an id is accepted, the main menu is displayed as shown in the
sample run. You can enter a choice of 1 for viewing the current balance, 2 for withdrawing money, 3 for depositing
money, and 4 for exiting the main menu. Once you exit, the system will prompt for an id again. So, once the system
starts, it won’t stop.
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


NUMBER_OF_ACCOUNTS = 10
INIT_BALANCE = 100


class Account:

    def __init__(self, id: int = 0, balance: float = 100, annualInterestRate: float = 0):
        """
        Default constructor for Account class objects
        :param id: Bank account unique identifier
        :param balance: Bank account current balance
        :param annualInterestRate: Bank account annual interest rate (percentage-based)
        """
        self.__id = id
        self.__balance = balance
        self.__annualInterestRate = annualInterestRate

    def getId(self):
        """
        Accessor function for bank account ID
        :return: bank account ID
        """
        return self.__id

    def setId(self, id: int):
        """
        Mutator function for bank account ID
        :param id: the new value for the bank account ID
        :return: Always returns True
        """
        self.__id = id
        return True

    def getBalance(self):
        """
        Access function for bank account balance
        :return: bank account balance
        """
        return self.__balance

    def setBalance(self, balance: float):
        """
        Mutator function for bank account balance
        :param balance: the new bank account balance
        :return: Always return True
        """
        self.__balance = balance
        return True

    def getAnnualInterestRate(self):
        """
        Accessor function for bank account's annual interest rate (APR)
        :return: bank account APR
        """
        return self.__annualInterestRate

    def setAnnualInterestRate(self, annual_interest_rate: float):
        """
        Mutator function for APR
        :param annual_interest_rate: the bank account's new APR value
        :return: Always return True
        """
        self.__annualInterestRate = annual_interest_rate
        return True

    def getMonthlyInterestRate(self):
        """
        Accessor function for bank account's monthly interest rate (MPR)
        :return: bank account MPR
        """
        return self.__annualInterestRate / 100 / 12

    def getMonthlyInterest(self):
        """
        Accessor function for bank account's current monthly interest accumulation
        :return: bank account's current monthly interest accumulation
        """
        return self.__balance * self.getMonthlyInterestRate()

    def withdraw(self, amount: float):
        """
        Mutator function to withdraw funds from the bank account
        :param amount: total monetary amount to deduct from the current bank account balance
        :return: the bank account balance after the withdraw transaction
        """
        self.__balance -= amount
        return amount

    def deposit(self, amount: float):
        """
        Mutator function to add funds from the bank account
        :param amount: total monetary amount to add to the current bank account balance
        :return: the bank account balance after the deposit transaction
        """
        self.__balance += amount
        return self.__balance


def main():
    # List of accounts
    accounts = []

    # Create ten accounts
    for accountID in range(0, NUMBER_OF_ACCOUNTS):
        accounts.append(Account(accountID, INIT_BALANCE))

    # Start the ATM
    while True:

        # Prompt user to access a bank account by ID number.
        accountID = int(input("\n Enter Account ID: "))

        # Ensure user-defined account ID is valid.
        while accountID < 0 or accountID > NUMBER_OF_ACCOUNTS-1:
            accountID = int(input("\n Invalid ID.\nEnter Account ID: "))

        # Entered valid account ID. Now enter account action (view, withdraw, deposit, or exit).
        while True:

            # Main Menu
            print("MainMenu\n{}\n{}\n{}\n{}\n".format("1: Check Balance", "2: Withdraw", "3: Deposit", "4: Exit"))

            # Reading selection
            userDefinedAccountID = int(input("\n Enter your selection: "))

            # Fetch the Account object whose account-ID matches the user-defined account ID
            for acc in accounts:
                # Comparing account id
                if acc.getId() == accountID:
                    selectedAccount = acc
                    break

            # View Balance
            if userDefinedAccountID == 1:
                # Printing balance
                print("The balance is $" + str(selectedAccount.getBalance()))

            # Withdraw
            elif userDefinedAccountID == 2:
                # Reading amount
                amt = float(input("\n Enter amount to withdraw: "))
                # Calling withdraw method
                selectedAccount.withdraw(amt)
                # Printing updated balance
                print("\n Updated Balance: " + str(selectedAccount.getBalance()) + " \n")

            # Deposit
            elif userDefinedAccountID == 3:
                # Reading amount
                amt = float(input("\n Enter amount to deposit: "))
                # Calling deposit method
                selectedAccount.deposit(amt)
                # Printing updated balance
                print("\n Updated Balance: " + str(selectedAccount.getBalance()) + " \n")

            # Any other choice
            else:
                break


# Make script executable as a python program
if __name__ == "__main__":
    main()
