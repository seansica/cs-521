#!/usr/bin/env python

"""
6.14.py: (Estimate Pi)

Pi can be computed using the following series:
m(i) = 4 * (1 - (1/3) + (1/5) - (1/7) + (1/9) - (1/11) + ... + ((-1^i+1)/(2*i-1)) )

Write a function that returns m(i) for a given i and write a test program that displays the following table:
i: m(i),
1: 4.0000,
101: 3.1515,
201: 3.1466,
301: 3.1449,
401: 3.1441,
501: 3.1436,
601: 3.1433,
701: 3.1430,
801: 3.1428,
901: 3.1427
"""

# import built-in libraries
import math
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"

# Generate list of numbers to test
TEST_DEGREES = [1, 101, 201, 301, 401, 501, 601, 701, 801, 901]


def compute_pi(degree: int):
    """
    Computes pi to the nth degree
    :param degree: Degree of accuracy to which Pi should be calculated
    :return: Pi
    """
    pi = 0
    # Calculate the sequence of decreasing rationals in the following
    # sequence: 1 - (1/3) + (1/5) - (1/7) + (1/9) - (1/11) + ... + ((-1^i+1)/(2*i-1))
    for i in range(1, degree+1):
        pi += math.pow(-1, i + 1) / (2 * i - 1)
    # Calculate and return Pi
    return 4 * pi


def main():
    # Print the header
    print("i\tm(i)")
    # Iterate over all test degrees
    for i in TEST_DEGREES:
        # Calculate pi
        pi = compute_pi(i)
        # Pretty print to console
        print(str(i) + "\t" + str(pi))


# Make script executable as a python program
if __name__ == "__main__":
    main()
