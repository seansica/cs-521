#!/usr/bin/env python

"""
11.40.py: (Guess the capitals)

Write a program that repeatedly prompts the user to enter a capital for a state. Upon receiving the user input, the
program reports whether the answer is correct. Assume that 50 states and their capitals are stored in a two-dimensional
list, as shown in Figure 11.13. The program prompts the user to answer all the states’ capitals and displays the total
correct count. The user’s answer is not case sensitive. Implement the program using a list to represent the data in the
following table.
"""

# import built-in libraries
import enum
import random
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class Capitals(enum.Enum):
    AL = ["Alabama", "Montgomery"]
    AK = ["Alaska", "Juneau"]
    AZ = ["Arizona", "Phoenix"]
    AR = ["Arkansas", "Little Rock"]
    CA = ["California",	"Sacramento"]
    CO = ["Colorado", "Denver"]
    CT = ["Connecticut", "Hartford"]
    DE = ["Delaware", "Dover"]
    FL = ["Florida", "Tallahassee"]
    GA = ["Georgia", "Atlanta"]
    HI = ["Hawaii", "Honolulu"]
    ID = ["Idaho", "Boise"]
    IL = ["Illinois", "Springfield"]
    IN = ["Indiana", "Indianapolis"]
    IA = ["Iowa", "Des Moines"]
    KS = ["Kansas", "Topeka"]
    KY = ["Kentucky", "Frankfort"]
    LA = ["Louisiana", "Baton Rouge"]
    ME = ["Maine", "Augusta"]
    MD = ["Maryland", "Annapolis"]
    MA = ["Massachusetts", "Boston"]
    MI = ["Michigan", "Lansing"]
    MN = ["Minnesota", "Saint Paul"]
    MS = ["Mississippi", "Jackson"]
    MO = ["Missouri", "Jefferson City"]
    MT = ["Montana", "Helena"]
    NE = ["Nebraska", "Lincoln"]
    NV = ["Nevada",	"Carson City"]
    NH = ["New Hampshire", "Concord"]
    NJ = ["New Jersey",	"Trenton"]
    NM = ["New Mexico",	"Santa Fe"]
    NY = ["New York", "Albany"]
    NC = ["North Carolina",	"Raleigh"]
    ND = ["North Dakota", "Bismarck"]
    OH = ["Ohio", "Columbus"]
    OK = ["Oklahoma", "Oklahoma City"]
    OR = ['Oregon',	"Salem"]
    PA = ["Pennsylvania", "Harrisburg"]
    RI = ["Rhode Island", "Providence"]
    SC = ["South Carolina", "Columbia"]
    SD = ["South Dakota", "Pierre"]
    TN = ["Tennessee", "Nashville"]
    TX = ["Texas", "Austin"]
    UT = ["Utah", "Salt Lake City"]
    VT = ["Vermont", "Montpelier"]
    VA = ["Virginia", "Richmond"]
    WA = ["Washington",	"Olympia"]
    WV = ["West Virginia", "Charleston"]
    WI = ["Wisconsin", "Madison"]
    WY = ["Wyoming", "Cheyenne"]

    @classmethod
    def getAll(cls):
        """
        Gets a 2D-array of all enum elements consisting of all 50 state names and state capitals.
        :return: Returns a 2D-array, array2d[x][y], where x refers to the state name, and y refers to the state capital
        """
        array2d = []
        for state in cls:
            array2d.append(state.value)
        return array2d

    @classmethod
    def getCapital(cls, target_state):
        """
        Gets the capital name that corresponds to a given state name
        :param target_state: refers to a user-specified state name
        :return: Returns the state capital if there is a match, otherwise returns -1
        """
        for state in cls:
            if state.value[0] == target_state:
                return state.value[1]
        return -1

    @classmethod
    def getState(cls, target_capital):
        """
        Gets the state name that corresponds to a given capital
        :param target_capital: Refers to a user-specified state capital
        :return: Returns the state name if there is a match, otherwise returns -1
        """
        for state in cls:
            if state.value[1] == target_capital:
                return state.value[0]
        return -1

    @classmethod
    def getRandom(cls):
        """
        Randomly selects a state from the enum of all 50 states
        :return: Returns a randomly selected state list element of state name and state capital
        """
        random_seed = random.randint(0, 49)
        all_states = cls.getAll()
        return all_states[random_seed]


def main():
    usa = Capitals.getAll()

    # Set baseline counters
    correct = 0
    wrong = 0

    # Iterate over all 50 states
    for state in usa:

        # Prompt the user to enter a capital
        user_defined_capital = input("Enter the state capital of " + state[0] + ":")

        # Provide an escape key to prematurely end the program
        if user_defined_capital == "!":
            print("Score:", "[Correct:", correct, ", Wrong:", wrong, "]")
            print("Goodbye!")
            break

        # Determine if the user guess is correct (ignore case)
        if user_defined_capital.lower() == state[1].lower():
            # Guess correct! Increment correct count
            correct += 1
            print("Correct!")
            # print("Score:", "[Correct:", correct, ", Wrong:", wrong, "]")

        else:
            # Guess incorrect! Retry.
            wrong += 1
            print("Wrong!")
            # print("Score:", "[Correct:", correct, ", Wrong:", wrong, "]")

    # Game over when all 50 states have been guessed!
    print("The number of correct guesses was", correct)
    print("The number of incorrect guesses was", wrong)


# Make script executable as a python program
if __name__ == "__main__":
    main()
