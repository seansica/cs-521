#!/usr/bin/env python

"""
6.29.py: (Financial: credit card number validation)

Credit card numbers follow certain patterns: It must have between 13 and 16 digits, and the number must start with:
    - 4 for Visa cards
    - 5 for MasterCard credit cards
    - 37 for American Express cards
    - 6 for Discover cards

In 1954, Hans Luhn of IBM proposed an algorithm for validating credit card numbers. The algorithm is useful to determine
whether a card number is entered correctly or whether a credit card is scanned correctly by a scanner. Credit card
numbers are generated following this validity check, commonly known as the Luhn check or the Mod 10 check, which can be
described as follows (for illustration, consider the card number 4388576018402626):

1. Double every second digit from right to left. If doubling of a digit results in a two-digit number, add up the two
digits to get a single-digit number.
    4388576018402626
    2 * 2 = 4
    2 * 2 = 4
    4 * 2 = 8
    1 * 2 = 2
    6 * 2 = 12 (1 + 2 = 3)
    5 * 2 = 10 (1 + 0 = 1)
    8 * 2 = 16 (1 + 6 = 7)
    4 * 2 = 8

2. Now add all single-digit numbers from Step 1.
    4 + 4 + 8 + 2 + 3 + 1 + 7 + 8 = 37

3. Add all digits in the odd places from right to left in the card number.
    6 + 6 + 0 + 8 + 0 + 7 + 8 + 3 = 38

4. Sum the results from Steps 2 and 3.
    37 + 38 = 75

5. If the result from Step 4 is divisible by 10, the card number is valid; otherwise, it is invalid. For example, the
number 4388576018402626 is invalid, but the number 4388576018410707 is valid.

Write a program that prompts the user to enter a credit card number as an integer. Display whether the number is valid
or invalid. Design your program to use the following functions:

    # Return true if the card number is valid
    def isValid(number):

    # Get the result from Step 2
    def sumOfDoubleEvenPlace(number):

    # Return this number if it is a single digit, otherwise, return # the sum of the two digits
    def getDigit(number):

    # Return sum of odd place digits in number
    def sumOfOddPlace(number):

    # Return true if the digit d is a prefix for number
    def prefixMatched(number, d):

    # Return the number of digits in d
    def getSize(d):

    # Return the first k number of digits from number. If the number of digits in number is less than k, return number.
    def getPrefix(number, k):
"""

# import built-in libraries
import enum

# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class CardTypes(enum.Enum):
    """
    Enum to store all possible credit card types based on prefix
    """
    VISA = 4
    MASTERCARD = 5
    AMEX = 37
    DISCOVER = 6

    @classmethod
    def values(cls):
        """
        Returns a list of all possible prefix values
        :return: Returns a list of all known CC prefix values
        """
        return [s.value for s in cls]

    @classmethod
    def getCardType(cls, cc_num):
        """
        Searches the enum for the prefix that matches the given CC number
        :param cc_num: The given credit card number
        :return: Returns the credit card type that corresponds to the matched prefix. Returns -1 if no match found.
        """
        for t in cls:
            if prefixMatched(cc_num, t.value):
                return t
        return -1


def isValid(number):
    """
    Determines whether a given CC number is valid according to the Hans Luhn algorithm
    :param number: Refers to a given credit card number (not necessarily valid)
    :return: Return True if the card number is valid
    """
    # Credit card numbers follow certain patterns: It must have between 13 and 16 digits, and the number must start with
    # - 4 for Visa cards
    # - 5 for MasterCard credit cards
    # - 37 for American Express cards
    # - 6 for Discover cards
    cc_type = CardTypes.getCardType(number)
    # print(cc_type)

    # Double every second digit from right to left. If doubling of a digit results in a two-digit number, add up the two
    # digits to get a single-digit number.
    step1_2 = sumOfDoubleEvenPlace(number)
    # print("SUM OF EVERY SECOND DIGIT=", sum_every_second_digit)

    # Add all digits in the odd places from right to left in the card number.
    step3 = sumOfOddPlace(number)
    # print("SUM OF ODDS=", sum_of_odd_places)

    # Sum the results from Steps 2 and 3.
    step4 = step1_2 + step3
    # print(step4)

    # If the result from Step 4 is divisible by 10, the card number is valid; otherwise, it is invalid.
    step5 = cc_type != -1 and step4 % 10 == 0
    # print(step5)

    return step5


def sumOfDoubleEvenPlace(number):
    """
    Double every second digit from right to left. If doubling of a digit results in a two-digit number, add up the two
    digits to get a single-digit number. Then adds all single-digit sums together.
    :param number: Refers to the CC number to be assessed
    :return: Returns the result of Step 2 of the Hans Luhn algorithm
    """
    # Create container to store every second digit, from right to left, of the CC number
    every_second_digit_from_right_to_left = []
    # Use a counter to match only every second number
    counter = 0
    for digit in str(number)[::-1]:
        # Match only numbers when counter is even, resulting in a match for every second number from right to left
        if (counter > 0) and (counter % 2 != 0):
            # Double the matched number
            double_digit = 2 * int(digit)
            # print("EVERY SECOND DIGIT=", digit, end='\t')
            # print("DOUBLE SECOND DIGIT=", double_digit, end='\n')
            # Store the matched number in the container
            every_second_digit_from_right_to_left.append(double_digit)
        # Increment the counter to ensure odd numbered digits are excluded
        counter += 1
    # print()

    sum_of_all = 0
    # Iterate over all matched numbers (that is, every other CC digit from right to left)
    for num in every_second_digit_from_right_to_left:
        sum_of_all += getDigit(num)

    return sum_of_all


def getDigit(number: int):
    """
    Return this number if it is a single digit, otherwise, return # the sum of the two digits
    :param number: Refers to the CC number that should be assessed
    :return: Returns this number if it is a single digit, otherwise, return # the sum of the two digits
    """
    # If number is two digits or more (e.g. 10, 11, 12), then add the digits together (e.g. 10: 1+0=1, 11: 1+1=2)
    if getSize(number) > 1:
        # print("DOUBLE SECOND DIGIT=", number, end='\t')
        summed_digits = 0
        while number > 0:
            rem = number % 10
            summed_digits += rem
            number = int(number / 10)
        # print("SUM OF DOUBLE DIGITS=", summed_digits, end='\n')
        return summed_digits
    else:
        # print("DOUBLE SECOND DIGIT=", num, end='\n')
        return number


def sumOfOddPlace(number: int):
    """
    Adds all digits in the odd places from right to left in the card number
    :param number: Refers to the CC number that should be scanned
    :return: Returns the sum of all odd-placed digits (from right to left) in the given CC number
    """
    # print("CC NUMBER=", number)
    sum_of_odd_digits = 0
    # Iterate over all integers in the CC number
    for index in range(0, len(str(number))):
        cc_digit = int(str(number)[index])
        # Match only odd numbered integers
        if index % 2 != 0:
            # print("ODD DIGIT=", cc_digit)
            # Match found! Add the odd numbered digit to the running total
            sum_of_odd_digits += cc_digit

    return sum_of_odd_digits


def prefixMatched(number, d):
    """
    Determines whether a given prefix, d, exists in number
    :param number: Refers to the CC number that should be searched
    :param d: Refers to the prefix that should be matched
    :return: Returns True if the digit d is a prefix for number, otherwise False
    """
    if str(number).startswith(str(d)):
        return True
    return False


def getSize(d):
    """
    Determines the number of characters that exist in d
    :param d: Refers to the number that should be scrutinized
    :return: Returns the total number of digits/chars specified by the d parameter
    """
    return len(str(d))


def getPrefix(number, k):
    """
    Return the first k number of digits from number. If the number of digits in number is less than k, return number.
    :param number: Refers to the number that should be searched
    :param k: Refers to the number of integers in number that should be matched
    :return: Returns the prefix in number as specified by 'k'
    """
    if getSize(number) < k:
        return number
    else:
        # print("NUMBER=", number, "k=", k, "PREFIX=", int(str(number)[:k]))
        return int(str(number)[:k])


def main():
    """
    Tests the Hans Luhn algorithm against valid and invalid CC numbers
    """
    invalid_cc = 4388576018402626
    valid_cc = 4388576018410707
    print(invalid_cc, ":", isValid(invalid_cc))
    print(valid_cc, ":", isValid(valid_cc))


# Make script executable as a python program
if __name__ == "__main__":
    main()
