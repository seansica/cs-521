#!/usr/bin/env python

"""
14.10.py: (Guess the capitals)

Rewrite Exercise 11.40 using a dictionary to store the pairs of states and capitals so that the questions are randomly
displayed.
"""

# import built-in libraries
import enum
import random
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


class Capitals(enum.Enum):
    AL = ["Alabama", "Montgomery"]
    AK = ["Alaska", "Juneau"]
    AZ = ["Arizona", "Phoenix"]
    AR = ["Arkansas", "Little Rock"]
    CA = ["California",	"Sacramento"]
    CO = ["Colorado", "Denver"]
    CT = ["Connecticut", "Hartford"]
    DE = ["Delaware", "Dover"]
    FL = ["Florida", "Tallahassee"]
    GA = ["Georgia", "Atlanta"]
    HI = ["Hawaii", "Honolulu"]
    ID = ["Idaho", "Boise"]
    IL = ["Illinois", "Springfield"]
    IN = ["Indiana", "Indianapolis"]
    IA = ["Iowa", "Des Moines"]
    KS = ["Kansas", "Topeka"]
    KY = ["Kentucky", "Frankfort"]
    LA = ["Louisiana", "Baton Rouge"]
    ME = ["Maine", "Augusta"]
    MD = ["Maryland", "Annapolis"]
    MA = ["Massachusetts", "Boston"]
    MI = ["Michigan", "Lansing"]
    MN = ["Minnesota", "Saint Paul"]
    MS = ["Mississippi", "Jackson"]
    MO = ["Missouri", "Jefferson City"]
    MT = ["Montana", "Helena"]
    NE = ["Nebraska", "Lincoln"]
    NV = ["Nevada",	"Carson City"]
    NH = ["New Hampshire", "Concord"]
    NJ = ["New Jersey",	"Trenton"]
    NM = ["New Mexico",	"Santa Fe"]
    NY = ["New York", "Albany"]
    NC = ["North Carolina",	"Raleigh"]
    ND = ["North Dakota", "Bismarck"]
    OH = ["Ohio", "Columbus"]
    OK = ["Oklahoma", "Oklahoma City"]
    OR = ['Oregon',	"Salem"]
    PA = ["Pennsylvania", "Harrisburg"]
    RI = ["Rhode Island", "Providence"]
    SC = ["South Carolina", "Columbia"]
    SD = ["South Dakota", "Pierre"]
    TN = ["Tennessee", "Nashville"]
    TX = ["Texas", "Austin"]
    UT = ["Utah", "Salt Lake City"]
    VT = ["Vermont", "Montpelier"]
    VA = ["Virginia", "Richmond"]
    WA = ["Washington",	"Olympia"]
    WV = ["West Virginia", "Charleston"]
    WI = ["Wisconsin", "Madison"]
    WY = ["Wyoming", "Cheyenne"]

    @classmethod
    def getAll(cls):
        """
        Gets a 2D-array of all enum elements consisting of all 50 state names and state capitals.
        :return: Returns a 2D-array, array2d[x][y], where x refers to the state name, and y refers to the state capital
        """
        array2d = []
        for state in cls:
            array2d.append(state.value)
        return array2d

    @classmethod
    def getAllDict(cls):
        """
        Gets a dictionary of all enum elements
        :return: Returns a dictionary where states are keys and capitals are values
        """
        return dict(cls.getAll())

    @classmethod
    def getCapital(cls, target_state):
        """
        Gets the capital name that corresponds to a given state name
        :param target_state: refers to a user-specified state name
        :return: Returns the state capital if there is a match, otherwise returns -1
        """
        for state in cls:
            if state.value[0] == target_state:
                return state.value[1]
        return -1

    @classmethod
    def getState(cls, target_capital):
        """
        Gets the state name that corresponds to a given capital
        :param target_capital: Refers to a user-specified state capital
        :return: Returns the state name if there is a match, otherwise returns -1
        """
        for state in cls:
            if state.value[1] == target_capital:
                return state.value[0]
        return -1

    @classmethod
    def getRandom(cls):
        """
        Randomly selects a state from the enum of all 50 states
        :return: Returns a randomly selected state list element of state name and state capital
        """
        random_seed = random.randint(0, 49)
        all_states = cls.getAll()
        return all_states[random_seed]


def main():
    usa = Capitals.getAllDict()

    # Set baseline counters
    correct = 0
    wrong = 0

    # Iterate until all 50 states have been guessed
    while len(usa) >= 1:
        # Generate a random index number from 0 to the length of the array of states and pick a random state
        state, capital = random.choice(list(usa.items()))

        # Prompt the user to enter a capital
        user_defined_capital = input("Enter the state capital of " + state + ":")

        # Provide an escape key to prematurely end the program
        if user_defined_capital == "!":
            print("Score:", "[Correct:", correct, ", Wrong:", wrong, "]")
            print("Goodbye!")
            break

        # Determine if the user guess is correct (ignore case)
        if user_defined_capital.lower() == capital.lower():
            # Guess correct! Increment correct count and remove state from subsequent pulls
            correct += 1
            usa.pop(state)
            print("Correct!")
            # print("Score:", "[Correct:", correct, ", Wrong:", wrong, "]")

        else:
            # Guess incorrect! Retry. (but still pop the state; no repeats!)
            wrong += 1
            usa.pop(state)
            print("Wrong!")
            # print("Score:", "[Correct:", correct, ", Wrong:", wrong, "]")

    # Game over when all 50 states have been guessed!
    print("The number of correct guesses was", correct)
    print("The number of incorrect guesses was", wrong)


# Make script executable as a python program
if __name__ == "__main__":
    main()
