#!/usr/bin/env python

"""
6.25.py: (Emirp)

An emirp ( prime spelled backward) is a non-palindromic prime number whose reversal is also a prime.
For example, both 17 and 71 are prime numbers, so 17 and 71 are emirps. Write a program that displays the first 100
emirps.

Display 10 numbers per line and align the numbers properly, as follows:
 13  17  31  37  71  73  79  97 107 113
149 157 167 179 199 311 337 347 359 389 ...
"""

# import built-in libraries
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


MAX_PER_LINE = 10  # Determines how many emirp values to print per line
CHECK_RANGE = 400  # Determines the range of numbers to check from 1 to CHECK_RANGE, inclusive


def is_prime(number: int):
    """
    Determines whether a given number is prime
    :param number: The integer to be analyzed
    :return: True if prime, otherwise False
    """
    # Return False if number equals 0, 1, or 2; otherwise proceed with prime check
    if number > 2:
        # Check if number is divisible by any integer from 2 up to the number, exclusive
        for i in range(2, number):
            if number % i == 0:
                # Match found! Number is divisible by i, therefore number is not prime
                return False
        # No matches found! Number is not prime.
        return True
    else:
        # Number equals 0, 1, or 2, therefore is not prime
        return False


def is_emirp(number: int):
    """
    Determines whether both number and its reverse are both prime numbers
    :param number: the number to be scrutinized
    :return: True if the number and its reverse are both prime numbers, otherwise False
    """
    # Check if number is prime
    if is_prime(number):
        # Number is prime! Proceed with reverse check
        reverse = int(str(number)[::-1])
        # Check if reverse number is prime
        if is_prime(reverse):
            # Reverse number is prime! Therefore both numbers are emirps!
            return True
    # Number is not prime, therefore number is not an emirp
    return False


def main():
    # Set a counter to track the number of emirps printed per line on the console
    emirps_per_line = 0
    # Iterate over all numbers from 1 to the max range as defined by the global var CHECK_RANGE, inclusive
    for i in range(1, CHECK_RANGE+1):
        # Check if each number is an emirp
        if is_emirp(i):
            # Number is an emirp! Print to console!
            if emirps_per_line == MAX_PER_LINE:
                # Start a newline after the 10th emirp has been printed
                print(str(i), end="\n")
                # Reset the line counter
                emirps_per_line = 0
            # Number is an emirp! Print to console!
            else:
                # Print the emirp to the current line post-pended with a tab character for readability
                print(str(i), end="\t")
                emirps_per_line += 1
    return


# Make script executable as a python program
if __name__ == "__main__":
    main()
