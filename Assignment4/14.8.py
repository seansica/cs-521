#!/usr/bin/env python

"""
14.8.py: (Display non-duplicate words in ascending order)

Write a program that prompts the user to enter a text file, reads words from the file, and displays all the
non-duplicate words in ascending order.
"""

# import built-in libraries
import sys
import re
# import third party libraries

__author__ = "Sean Sica"
__version__ = "1.0.0"
__maintainer__ = "Sean Sica"
__email__ = "sica@bu.edu"
__status__ = "Submitted"


def main():

    # Prompt the user for a filename and try to open
    try:
        # filename = "input.txt"  # Use for testing
        filename = input("Enter file name:")
        file = open(filename, 'r')

    # If the filename does not exist, print error and end the program
    except FileNotFoundError as error:
        print(repr(error))
        sys.exit(0)

    # Create a placeholder container to store all of the words in the file
    words = []

    # Capture all words in file and store in container
    for line in file:
        # Use RegEx to only capture alphanumeric characters so we only match words
        line = re.sub(r'[^A-Za-z0-9 ]+', '', line)

        # Add each word to the list
        words.extend(line.split())

    # Convert each word to lowercase to account for variations of same word
    words[:] = [word.lower() for word in words]

    # Remove duplicate words by converting words to a set, which by nature cannot contain duplicate elements
    dedup = set(words)

    # Alphabetically sort all of the words
    sorted_words = sorted(dedup)

    # Print final product to console
    for word in sorted_words:
        print(word)


# Make script executable as a python program
if __name__ == "__main__":
    main()
